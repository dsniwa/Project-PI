<?php
// Cek status login
if(empty($_SESSION['UIDUser'])){
header("HTTP/1.1 404 Not Found");
die();
}
    if(isset($_POST['editPosting'])){
        $idPosting = $_POST['idPosting'];
        $isiPosting = $_POST['isiPosting'];

        // Edit acara
        $query = $mysqli->prepare("UPDATE posting SET isi=? WHERE posting_id = ? AND pengguna_id = ?");
        $query->bind_param('sss', $isiPosting, $idPosting, $userID);
        if($query->execute()){
            $status="1";
            $pesan="Postingan berhasil diubah. <a href='/ketuplak/posting/view/'>Kembali ke daftar posting</a>";
        }else{
            $status="2";
            $pesan="Postingan gagal diubah.";
        }
        $query->close();
    }
?>
