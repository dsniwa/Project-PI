<?php
// Cek status login
if(empty($_SESSION['UIDUser'])){
header("HTTP/1.1 404 Not Found");
die();
}
    if(isset($_POST['editAcara'])){
        $idAcara = $_POST['idAcara'];
        $namaAcara = $_POST['namaAcara'];
        $lokasiAcara = $_POST['lokasiAcara'];
        $tanggalAcara = $_POST['tanggalAcara'];
        $waktuAcara = $_POST['mulaiAcara'].' - '.$_POST['selesaiAcara'];
        $kuotaAcara = $_POST['kuotaAcara'];
        $jenisAcara = $_POST['jenisAcara'];

        // Edit acara
        $query = $mysqli->prepare("UPDATE acara SET nama_acara=?, lokasi_acara=?, tgl_acara=?, waktu_acara=?, kuota=?, jenis=? WHERE acara_id = ? AND pengguna_id = ?");
        $query->bind_param('ssssssss', $namaAcara, $lokasiAcara, $tanggalAcara, $waktuAcara, $kuotaAcara, $jenisAcara, $idAcara, $userID);
        if($query->execute()){
            $status="1";
            $pesan="Acara berhasil diubah. <a href='/ketuplak/events/view/'>Kembali ke daftar acara</a>";
        }else{
            $status="2";
            $pesan="Acara gagal diubah.";
        }
        $query->close();

        //Ubah status berdasarkan kuota baru
        $ch = statusAcaraEdit($idAcara,$kuotaAcara);
    }
?>
