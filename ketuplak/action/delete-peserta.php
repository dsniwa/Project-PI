<?php
// sertakan berkas utama
$role = "user";
require_once $_SERVER['DOCUMENT_ROOT'].'/include/load.php';

// Cek status login
if(empty($_SESSION['UIDUser'])){
header("HTTP/1.1 404 Not Found");
die();
}
    // String
    $idPeserta = $_POST['id'];
    $status = 0;

    // Delete peserta
    $query=$mysqli->prepare("DELETE FROM peserta WHERE peserta_id=? AND pengguna_id = ?");
    $query->bind_param('ss', $idPeserta, $userID);
    $query->execute();
    if($query->execute()){
        $status = $status + 1;
    }
    $query->close();

    if ($status == 1) { 
        $response_array['status'] = 'success';
    } else {
        $response_array['status'] = 'failed';
    }
    header('Content-type: application/json');
    echo json_encode($response_array);
?>
