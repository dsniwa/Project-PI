    <section>
        <!-- Left Sidebar -->
        <aside id="leftsidebar" class="sidebar">
            <!-- User Info -->
            <div class="user-info">
                <div class="image">
                    <img src="/ketuplak/images/user-photos/user.png" width="48" height="48" alt="User" />
                </div>
                <div class="info-container">
                    <div class="name" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><?php echo $userNama ?></div>
                    <div class="email"><?php echo $userEmail ?></div>
                    <div class="btn-group user-helper-dropdown">
                        <i class="material-icons" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">keyboard_arrow_down</i>
                        <ul class="dropdown-menu pull-right">
                            <li><a href="/ketuplak/profile/"><i class="material-icons">person</i>Profile</a></li>
                            <!--li role="seperator" class="divider"></li-->
                            <li><a href="?logout"><i class="material-icons">input</i>Sign Out</a></li>
                        </ul>
                    </div>
                </div>
            </div>
            <!-- #User Info -->
            <!-- Menu -->
            <div class="menu">
                <ul class="list">
                    <li class="header">NAVIGASI</li>
                    <li <?php if($_GET['page']=="dashboard"){echo 'class="active"';} ?>>
                        <a href="/ketuplak/dashboard/">
                            <i class="material-icons">dashboard</i>
                            <span>Dashboard</span>
                        </a>
                    </li>
                    <li <?php if($_GET['page']=="events"){echo 'class="active"';} ?>>
                        <a href="/ketuplak/events/view/">
                            <i class="material-icons">event</i>
                            <span>Acara</span>
                        </a>
                    </li>
                    <li <?php if($_GET['page']=="posting"){echo 'class="active"';} ?>>
                        <a href="/ketuplak/posting/view/">
                            <i class="material-icons">featured_play_list</i>
                            <span>Posting</span>
                        </a>
                    </li>
                    <li <?php if($_GET['page']=="participants"){echo 'class="active"';} ?>>
                        <a href="javascript:void(0);" class="menu-toggle">
                            <i class="material-icons">group</i>
                            <span>Peserta</span>
                        </a>
                        <ul class="ml-menu">
                            <li <?php if($_GET['page']=="participants" && $acaraID=="all"){echo 'class="active"';} ?>>
                                <a href="/ketuplak/participants/all/">
                                    <span>Semua Peserta</span>
                                </a>
                            </li>
                            <?php
                            $query=$mysqli->prepare('SELECT acara_id,nama_acara FROM `acara` WHERE `pengguna_id` = ?');
                            $query->bind_param('s', $userID);
                            $query->execute();
                            $result=$query->get_result();

                            while($row = $result->fetch_array()){
                            ?>
                            <li <?php if($_GET['page']=="participants" && $acaraID==$row['acara_id']){echo 'class="active"';} ?>>
                                <a href="/ketuplak/participants/<?php echo $row['acara_id'] ?>/">
                                    <span><?php echo $row['nama_acara'] ?></span>
                                </a>
                            </li>
                            <?php
                            }
                            ?>
                        </ul>
                    </li>

                    <li <?php if($_GET['page']=="active-participants"){echo 'class="active"';} ?>>
                        <a href="javascript:void(0);" class="menu-toggle">
                            <i class="material-icons">group_add</i>
                            <span>Peserta Aktif</span>
                        </a>
                        <ul class="ml-menu">
                            <?php
                            $query=$mysqli->prepare('SELECT acara_id,nama_acara FROM `acara` WHERE `pengguna_id` = ?');
                            $query->bind_param('s', $userID);
                            $query->execute();
                            $result=$query->get_result();

                            while($row = $result->fetch_array()){
                            ?>
                            <li <?php if($_GET['page']=="active-participants" && $acaraID==$row['acara_id']){echo 'class="active"';} ?>>
                                <a href="/ketuplak/active-participants/<?php echo $row['acara_id'] ?>/">
                                    <span><?php echo $row['nama_acara'] ?></span>
                                </a>
                            </li>
                            <?php
                            }
                            ?>
                        </ul>
                    </li>
                </ul>
            </div>
            <!-- #Menu -->
        </aside>
        <!-- #END# Left Sidebar -->
    </section>
