<!-- Head -->
<?php include "head.php"; ?>
    
    <!-- Navigation BAR -->
    <?php include "navbar.php"; ?>

    <section class="content">
        <div class="container-fluid">
            <!-- Exportable Table -->
            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <h2>
                                <?php
                                    $query=$mysqli->prepare('SELECT acara_id,nama_acara,jenis FROM `acara` WHERE `pengguna_id` = ? AND `acara_id` = ?');
                                    $query->bind_param('ss', $userID, $acaraID);
                                    $query->execute();
                                    $result=$query->get_result();
                                    while($row = $result->fetch_array()){
                                        echo "Daftar Peserta Aktif ".$row['nama_acara'];
                                        $jenis_acara = $row['jenis'];
                                    }
                                ?>
                            </h2>
                        </div>
                        <div class="body">
                            <table style="width: 100%;" class="table table-bordered table-striped table-hover dataTable table-exportable nowrap">
                                <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>NPM / No. Identitas</th>
                                        <th>Nama</th>
                                        <th>Domisili</th>
                                        <?php
                                        if($jenis_acara == 0){
                                            echo "<th>Kelas</th><th>Jurusan</th>";
                                        }else{
                                            echo "<th>Alamat</th><th>Pekerjaan</th>";
                                        }
                                        ?>
                                        <th>Nomor HP</th>
                                        <th>Email</th>
                                        <th>Tanggal Lahir</th>
                                        <th>Waktu Aktifasi</th>
                                    </tr>
                                </thead>
                                <tbody>
                                <?php
                                    if($acaraID != 'all'){
                                        $query=$mysqli->prepare('SELECT peserta.*,acara.nama_acara FROM `peserta` INNER JOIN acara ON acara.acara_id=peserta.acara_id WHERE acara.acara_id = ? AND acara.pengguna_id = ? AND peserta.status = "1"');
                                        $query->bind_param('ss', $acaraID, $userID);
                                    }else{
                                        $query=$mysqli->prepare('SELECT peserta.*,acara.nama_acara FROM `peserta` INNER JOIN acara ON acara.acara_id=peserta.acara_id WHERE acara.pengguna_id = ? AND peserta.status = "1"');
                                        $query->bind_param('s', $userID);
                                    }
                                    $query->execute();
                                    $result=$query->get_result();

                                    $no = 0;
                                    while($row = $result->fetch_array()){
                                        $no++;
                                        echo '
                                            <tr>
                                                <td>'.$no.'</td>
                                                <td>'.$row['npm'].'</td>
                                                <td>'.$row['nama'].'</td>
                                                <td>'.$row['domisili'].'</td>
                                                <td>'.$row['var_satu'].'</td>
                                                <td>'.$row['var_dua'].'</td>
                                                <td>'.$row['no_hp'].'</td>
                                                <td>'.$row['email'].'</td>
                                                <td>'.$row['tgl_lahir'].'</td>
                                                <td>'.$row['waktu_aktifasi'].'</td>
                                            </tr>
                                        ';
                                    }
                                    ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <!-- #END# Exportable Table -->

        </div>
    </section>
    
    <!-- Jquery Core Js -->
    <script src="/dash/plugins/jquery/jquery.min.js"></script>

    <!-- Bootstrap Core Js -->
    <script src="/dash/plugins/bootstrap/js/bootstrap.js"></script>

    <!-- Slimscroll Plugin Js -->
    <script src="/dash/plugins/jquery-slimscroll/jquery.slimscroll.js"></script>

    <!-- Waves Effect Plugin Js -->
    <script src="/dash/plugins/node-waves/waves.js"></script>

    <!-- Jquery DataTable Plugin Js -->
    <script src="/dash/plugins/jquery-datatable/jquery.dataTables.js"></script>
    <script src="/dash/plugins/jquery-datatable/skin/bootstrap/js/dataTables.bootstrap.js"></script>
    <script src="/dash/plugins/jquery-datatable/extensions/export/dataTables.buttons.min.js"></script>
    <script src="/dash/plugins/jquery-datatable/extensions/export/buttons.flash.min.js"></script>
    <script src="/dash/plugins/jquery-datatable/extensions/export/jszip.min.js"></script>
    <script src="/dash/plugins/jquery-datatable/extensions/export/pdfmake.min.js"></script>
    <script src="/dash/plugins/jquery-datatable/extensions/export/vfs_fonts.js"></script>
    <script src="/dash/plugins/jquery-datatable/extensions/export/buttons.html5.min.js"></script>
    <script src="/dash/plugins/jquery-datatable/extensions/export/buttons.print.min.js"></script>

    <script src="https://cdn.datatables.net/fixedcolumns/3.2.2/js/dataTables.fixedColumns.min.js"></script>

    <!-- Select Plugin Js -->
    <script src="/dash/plugins/bootstrap-select/js/bootstrap-select.js"></script>

    <!-- Autosize Plugin Js -->
    <script src="/dash/plugins/autosize/autosize.js"></script>

    <!-- Moment Plugin Js -->
    <script src="/dash/plugins/momentjs/moment.js"></script>

    <!-- Bootstrap Material Datetime Picker Plugin Js -->
    <script src="/dash/plugins/bootstrap-material-datetimepicker/js/bootstrap-material-datetimepicker.js"></script>
    
    <!-- TinyMCE -->
    <script src="/dash/plugins/tinymce/tinymce.js"></script>

    <!-- Custom Js -->
    <script src="/dash/js/admin.js"></script>
    <script src="/dash/js/jquery-datatable.js"></script>
    <script src="/dash/js/basic-form-elements.js"></script>
    <script src="/dash/js/tooltips-popovers.js"></script>
    <script src="/dash/js/editors.js"></script>
</body>

</html>