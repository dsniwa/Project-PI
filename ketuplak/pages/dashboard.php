﻿<!-- Head -->
<?php include "head.php"; ?>
    
    <!-- Navigation BAR -->
    <?php include "navbar.php"; ?>

    <section class="content">
        <div class="container-fluid">
            <?php
                $query=$mysqli->prepare('SELECT ganti_email FROM pengguna WHERE pengguna_id = ?');
                $query->bind_param('s', $userID);
                $query->execute();
                $result=$query->get_result();
                while($row = $result->fetch_array()){
                    $ganti_email = $row['ganti_email'];
                }
                $query->close();
            ?>

            <?php if($ganti_email != ""){ ?>
                <div class="alert bg-orange alert-dismissible" role="alert">
                    Email <?php echo $ganti_email; ?> sedang menunggu konfirmasi. Jika link konfirmasi Anda hilang atau tidak menerima link konfirmasi pada email Anda, silahkan <a href="/ketuplak/chresend.php" style="color:black;"> kirim ulang link konfirmasi</a>. Atau <a href="/ketuplak/revoke.php" style="color:black;"> batalkan ganti email</a>.
                </div>
            <?php } ?>
            <div class="block-header">
                <div class="row clearfix">
                    <div class="col-sm-6"><h2>DASHBOARD</h2></div>
                    <div class="col-sm-6"><span style="float: right;">Terakhir Login: <?php echo $_SESSION['last_loginUser']; ?></span></div>
                </div>
            </div>

            <!-- Widgets -->
            <div class="row clearfix">
                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                    <div class="info-box bg-pink hover-expand-effect">
                        <div class="icon">
                            <i class="material-icons">group</i>
                        </div>
                        <div class="content">
                            <div class="text">ACARA</div>
                            <?php
                            $db_handle = new DBController();
                            $query ="SELECT COUNT(id) FROM `acara` WHERE `pengguna_id` = '$userID'";
                            $results = $db_handle->countQuery($query);
                            ?>
                            <div class="number count-to" data-from="0" data-to="<?php echo $results; ?>" data-speed="15" data-fresh-interval="20"></div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                    <div class="info-box bg-cyan hover-expand-effect">
                        <div class="icon">
                            <i class="material-icons">event</i>
                        </div>
                        <div class="content">
                            <div class="text">PESERTA</div>
                            <?php
                            $db_handle = new DBController();
                            $query ="SELECT COUNT(id) FROM `peserta` WHERE `pengguna_id` = '$userID'";
                            $results = $db_handle->countQuery($query);
                            ?>
                            <div class="number count-to" data-from="0" data-to="<?php echo $results; ?>" data-speed="75" data-fresh-interval="20"></div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                    <div class="info-box bg-orange hover-zoom-effect">
                        <div class="icon">
                            <i class="material-icons">person_add</i>
                        </div>
                        <div class="content">
                            <div class="text">PENDAFTARAN</div>
                            <div class="text"><a href="/portal/registration/" target="_null" style="text-decoration: none; color: #fff;">Open Now <i class="material-icons" style="font-size: 13px;">open_in_new</i></a></div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row clearfix">
                <!-- Task Info -->
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <div class="card">
                        <div class="header">
                            <h2>10 Pendaftar Terakhir</h2>
                        </div>
                        <div class="body">
                            <div class="table-responsive">
                                <table class="table table-hover dashboard-task-infos">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>NPM / No.Identitas</th>
                                            <th>Nama</th>
                                            <th>Acara</th>
                                            <th>Email</th>
                                            <th>Status</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    <?php
                                    $query=$mysqli->prepare('SELECT peserta.*,acara.nama_acara FROM `peserta` INNER JOIN acara ON acara.acara_id=peserta.acara_id WHERE acara.pengguna_id = ? ORDER BY peserta.id DESC LIMIT 10');
                                    $query->bind_param('s', $userID);
                                    $query->execute();
                                    $result=$query->get_result();
                                    $no = 0;
                                    while($row = $result->fetch_array()){
                                        $no++;
                                        if($row['status']==1){
                                            $statusPeserta = "Aktif";
                                        }else{
                                            $statusPeserta = "Belum Aktif";
                                        }
                                        echo '
                                        <tr>
                                            <td>'.$no.'</td>
                                            <td>'.$row['npm'].'</td>
                                            <td>'.$row['nama'].'</td>
                                            <td>'.$row['nama_acara'].'</td>
                                            <td>'.$row['email'].'</td>
                                            <td>'.$statusPeserta.'</td>
                                        </tr>';
                                    }
                                    ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- #END# Task Info -->
            </div>
        </div>
    </section>
    
    <!-- Jquery Core Js -->
    <script src="/dash/plugins/jquery/jquery.min.js"></script>

    <!-- Bootstrap Core Js -->
    <script src="/dash/plugins/bootstrap/js/bootstrap.js"></script>

    <!-- Slimscroll Plugin Js -->
    <script src="/dash/plugins/jquery-slimscroll/jquery.slimscroll.js"></script>

    <!-- Waves Effect Plugin Js -->
    <script src="/dash/plugins/node-waves/waves.js"></script>

    <!-- Jquery CountTo Plugin Js -->
    <script src="/dash/plugins/jquery-countto/jquery.countTo.js"></script>

    <!-- Autosize Plugin Js -->
    <script src="/dash/plugins/autosize/autosize.js"></script>

    <!-- Custom Js -->
    <script src="/dash/js/admin.js"></script>
    <script src="/dash/js/index.js"></script>
</body>

</html>
