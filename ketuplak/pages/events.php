<!-- Head -->
<?php include "head.php"; ?>
    
    <!-- Navigation BAR -->
    <?php include "navbar.php"; ?>

    <section class="content">
        <div class="container-fluid">
            <?php if(isset($regStatus)){
                if($regStatus=="sukses"){
            ?>
                <div class="alert bg-green alert-dismissible" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                    Acara <strong style="color: blue;"><?php echo $namaAcara; ?></strong> berhasil ditambahkan.
                </div>
            <?php }else{ ?>
                <div class="alert bg-red alert-dismissible" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                    Acara baru gagal ditambahkan, silahkan coba lagi.
                </div>
            <?php } } ?>

            <?php if(isset($_GET['s'])){
                if($_GET['s']=="1"){ ?>
                    <div class="alert bg-green alert-dismissible" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                        <?php echo $_GET['mess']; ?>
                    </div>
                    <?php 
                } 
                if($_GET['s']=="2"){ ?>
                    <div class="alert bg-red alert-dismissible" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                        <?php echo $_GET['mess']; ?>
                    </div>
                    <?php
                }
            }?>
            <?php if(isset($_GET['x'])){
                $x = $_GET['x'];
            }else{
                $x= "";
            }

            if($x == "add"){
            ?>
            <!-- Exportable Table -->
            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <h2>TAMBAH ACARA</h2>
                        </div>
                        <div class="body">
                            <div class="row clearfix">
                                <div class="col-sm-12">
                                <form id="form_validation" method="POST" action="/ketuplak/events/view/">
                                    <div class="form-group form-float">
                                        <div class="form-line">
                                            <input type="text" class="form-control" name="namaAcara" required />
                                            <label class="form-label">Nama Acara</label>
                                        </div>
                                    </div>

                                    <div class="form-group form-float">
                                        <div class="form-line">
                                            <input type="text" class="form-control" name="lokasiAcara" required />
                                            <label class="form-label">Lokasi Acara</label>
                                        </div>
                                    </div>

                                    <div class="form-group form-float">
                                        <div class="form-line">
                                            <input type="text" class="datepicker form-control" name="tanggalAcara" value="00-00-0000" required />
                                            <label class="form-label">Tanggal Acara</label>
                                        </div>
                                    </div>
                                    
                                    <div class="row clearfix">
                                    <div class="col-sm-6" style="margin-bottom: 0px !important;">
                                        <div class="form-group form-float">
                                            <div class="form-line">
                                                <input type="text" class="timepicker form-control" name="mulaiAcara" value="00:00" required />
                                                <label class="form-label">Waktu Mulai</label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-6" style="margin-bottom: 0px !important;">
                                        <div class="form-group form-float">
                                            <div class="form-line">
                                                <input type="text" class="timepicker form-control" name="selesaiAcara" value="00:00" required />
                                                <label class="form-label">Waktu Selesai</label>
                                            </div>
                                        </div>
                                    </div>
                                    </div>

                                    <div class="form-group form-float">
                                        <div class="form-line">
                                            <input type="number" min="1" class="form-control" name="kuotaAcara" data-toggle="tooltip" data-placement="left" data-original-title="Hanya isi dengan angka positif." required />
                                            <label class="form-label">Kuota Peserta</label>
                                        </div>
                                        <div class="help-info">Isi dengan angka positif</div>
                                    </div>

                                    <div class="form-group form-float">
                                        <label>Jenis Acara</label>
                                        <select name="jenisAcara" class="form-control show-tick" required>
                                            <option value="" disabled selected>-- Please select --</option>
                                            <option value="0">Khusus Mahasiswa Gunadarma</option>
                                            <option value="1">Umum</option>
                                        </select>
                                    </div>

                                    <div class="form-group form-float">
                                        <div class="switch">
                                            Posting acara:<br/><label>TIDAK<input type="checkbox" name="postAcara" class="postAcara"><span class="lever"></span>YA</label>
                                        </div>
                                    </div>
                                    
                                    <div class="form-group form-float divPost">
                                        <textarea id="tinymce" name="isiPost">
                                            <h2>Masukkan isi postingan acara Anda di sini.</h2>
                                        </textarea>
                                        <h2 class="card-inside-title">Pilih salah satu gambar di bawah ini sebagai gambar header postingan Anda. (Header ini tidak akan bisa diubah, silahkan pilih dengan yakin)</h2>
                                        <div class="cc-selector-2">
                                            <?php
                                            $dir = $_SERVER['DOCUMENT_ROOT']."/images/cover";
                                            $images = glob("$dir/*.{jpg,png,bmp}", GLOB_BRACE);
                                            $no =0;

                                            foreach($images as $image)
                                            { $no++; ?>
                                                <input id="himg-<?php echo $no; ?>" type="radio" name="headerPost" value="/images/cover/<?php echo basename($image);  ?>" required/>
                                                <label class="drinkcard-cc himg-<?php echo $no; ?>" for="himg-<?php echo $no; ?>"></label>
                                            <?php }
                                            ?>      
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <input type="submit" name="newAcara" class="btn btn-block btn-primary m-t-15 waves-effect" />
                                    </div>
                                </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- #END# Exportable Table -->
            <?php
            }else{
            ?>
            <!-- Exportable Table -->
            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <h2>
                                DAFTAR ACARA
                            </h2>
                            <ul class="header-dropdown m-r--5" style="top: 15px !important;">
                                <a href="/ketuplak/events/add/" type="button" class="btn bg-deep-purple waves-effect" style="float: right;"><sapn>Tambahkan Acara</sapn></a>
                            </ul>
                        </div>
                        <div class="body">
                            <table style="width: 100%; vertical-align: bottom;" class="table table-bordered table-striped table-hover dataTable table-exportable">
                                <thead>
                                    <tr>
                                        <th class="middle">No</th>
                                        <th class="middle">Nama Acara</th>
                                        <th class="middle">Lokasi Acara</th>
                                        <th class="middle">Tanggal Acara</th>
                                        <th class="middle">Waktu Acara</th>
                                        <th class="middle">Peserta</th>
                                        <th class="middle">Kuota</th>
                                        <th class="middle">Jenis Acara</th>
                                        <th class="middle">Status Acara</th>
                                        <th class="middle" style="text-align:center;">Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                <?php
                                    //$sql = "SELECT * FROM admin";
                                    //$result = mysql_query($sql);

                                    $query=$mysqli->prepare('SELECT * FROM `acara` WHERE `pengguna_id` = ?');
                                    $query->bind_param('s', $userID);
                                    $query->execute();
                                    $result=$query->get_result();

                                    $no = 0;
                                    while($row = $result->fetch_array()){
                                        $no++;
                                        if($row['status']==1){
                                            $statusAcara = "<span style=\"color:green;\"><strong>Open</strong></span>";
                                        }else{
                                            $statusAcara = "<span style=\"color:red;\"><strong>Closed</strong></span>";
                                        }

                                        if($row['jenis']==0){
                                            $jenisAcara = "Khusus";
                                        }else{
                                            $jenisAcara = "Umum";
                                        }

                                        $idAcara = $row['acara_id'];

                                        $db_handle = new DBController();
                                        $query ="SELECT COUNT(id) FROM `peserta` WHERE `acara_id` = '$idAcara'";
                                        $jumlahPeserta = $db_handle->countQuery($query);

                                        if($jumlahPeserta >= $row['kuota']){
                                            $statusAcara = "<span style=\"color:red;\"><strong>Quota Full</strong></span>";
                                        }

                                        if($row['status']==1){
                                            $actButton='<a href="../close/'.$row['acara_id'].'" title="Close" class="btn btn-danger btn-xs waves-effect"><i class="material-icons">lock_outline</i></a>';
                                        }else if($jumlahPeserta >= $row['kuota']){
                                            $actButton='<a href="javascript:void(0)" style="cursor: not-allowed; color: gray;"  title="Disabled" class="btn bg-grey btn-xs waves-effect"><i class="material-icons">lock_open</i></a>';
                                        }else{
                                            $actButton='<a href="../open/'.$row['acara_id'].'" title="Open" class="btn btn-success btn-xs waves-effect"><i class="material-icons">lock_open</i></a>';
                                        }

                                        echo '
                                            <tr>
                                                <td class="middle">'.$no.'</td>
                                                <td class="middle">'.$row['nama_acara'].'</td>
                                                <td class="middle">'.$row['lokasi_acara'].'</td>
                                                <td class="middle">'.$row['tgl_acara'].'</td>
                                                <td class="middle">'.$row['waktu_acara'].'</td>
                                                <td class="middle">'.$jumlahPeserta.' Peserta</td>
                                                <td class="middle">'.$row['kuota'].' Peserta</td>
                                                <td class="middle">'.$jenisAcara.'</td>
                                                <td class="middle">'.$statusAcara.'</td>
                                                <td class="middle" style="text-align:center;" nowrap>
                                                    <a href="../edit/'.$row['acara_id'].'" title="Edit" class="btn btn-xs bg-blue waves-effect"><i class="material-icons">edit</i></a>
                                                    '.$actButton.'
                                                    <a href="javascript:void(0)" data-value="'.$row['acara_id'].'" onclick="dtDel(\''.$row['acara_id'].'\');" title="Delete" class="btn btn-danger btn-xs waves-effect"><i class="material-icons">delete</i></a><a href="javascript:void(0)" id="dt-del-btn"></a>
                                                </td>
                                            </tr>
                                        ';
                                    }
                                    ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <!-- #END# Exportable Table -->
            <?php } ?>
        </div>
    </section>
    
    <!-- Jquery Core Js -->
    <script src="/dash/plugins/jquery/jquery.min.js"></script>

    <!-- Bootstrap Core Js -->
    <script src="/dash/plugins/bootstrap/js/bootstrap.js"></script>

    <!-- Slimscroll Plugin Js -->
    <script src="/dash/plugins/jquery-slimscroll/jquery.slimscroll.js"></script>

    <!-- Waves Effect Plugin Js -->
    <script src="/dash/plugins/node-waves/waves.js"></script>

    <!-- Jquery Validation Plugin Js -->
    <script src="/dash/plugins/jquery-validation/jquery.validate.js"></script>
    <script src="/dash/plugins/jquery-validation/localization/messages_id.js"></script>

    <!-- Jquery DataTable Plugin Js -->
    <script src="/dash/plugins/jquery-datatable/jquery.dataTables.js"></script>
    <script src="/dash/plugins/jquery-datatable/skin/bootstrap/js/dataTables.bootstrap.js"></script>
    <script src="/dash/plugins/jquery-datatable/extensions/export/dataTables.buttons.min.js"></script>
    <script src="/dash/plugins/jquery-datatable/extensions/export/buttons.flash.min.js"></script>
    <script src="/dash/plugins/jquery-datatable/extensions/export/jszip.min.js"></script>
    <script src="/dash/plugins/jquery-datatable/extensions/export/pdfmake.min.js"></script>
    <script src="/dash/plugins/jquery-datatable/extensions/export/vfs_fonts.js"></script>
    <script src="/dash/plugins/jquery-datatable/extensions/export/buttons.html5.min.js"></script>
    <script src="/dash/plugins/jquery-datatable/extensions/export/buttons.print.min.js"></script>

    <script src="https://cdn.datatables.net/fixedcolumns/3.2.2/js/dataTables.fixedColumns.min.js"></script>

    <!-- Select Plugin Js -->
    <script src="/dash/plugins/bootstrap-select/js/bootstrap-select.js"></script>

    <!-- Autosize Plugin Js -->
    <script src="/dash/plugins/autosize/autosize.js"></script>

    <!-- Moment Plugin Js -->
    <script src="/dash/plugins/momentjs/moment.js"></script>

    <!-- Bootstrap Material Datetime Picker Plugin Js -->
    <script src="/dash/plugins/bootstrap-material-datetimepicker/js/bootstrap-material-datetimepicker.js"></script>
    
    <!-- TinyMCE -->
    <script src="/dash/plugins/tinymce/tinymce.js"></script>

    <!-- SweetAlert Plugin Js -->
    <script src="/dash/plugins/sweetalert/sweetalert.min.js"></script>

    <!-- Custom Js -->
    <script src="/dash/js/admin.js"></script>
    <script src="/dash/js/jquery-datatable.js"></script>
    <script src="/dash/js/ajax-func.js"></script>
    <script src="/dash/js/basic-form-elements.js"></script>
    <script src="/dash/js/form-validation.js"></script>
    <script src="/dash/js/tooltips-popovers.js"></script>
    <script src="/dash/js/editors.js"></script>
</body>

</html>
