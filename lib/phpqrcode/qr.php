<?php
/*
 * PHP QR Code encoder
 *
 * Exemplatory usage
 *
 * PHP QR Code is distributed under LGPL 3
 * Copyright (C) 2010 Dominik Dzienia <deltalab at poczta dot fm>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA
 */
    
    //html PNG location prefix
    $PNG_WEB_DIR = './temp/';

    include "qrlib.php";

    if (!isset($_GET['qr'])) die('Error :(');

    //it's very important!
    if (ctype_space($_GET['qr'])) {
        die('no space allowed!');
    }

    $d = trim($_GET['qr']);

    if (!file_exists($PNG_WEB_DIR))
        mkdir($PNG_WEB_DIR);
    
    $errorCorrectionLevel = 'H';
    $matrixPointSize = 10;
    $filename = $PNG_WEB_DIR.md5($d.'|'.$errorCorrectionLevel.'|'.$matrixPointSize).'.png';


    QRcode::png($d, $filename, $errorCorrectionLevel, $matrixPointSize, 2);    
    
    $im = imagecreatefrompng($filename);

    header('Content-Type: image/png');

    imagepng($im);
    imagedestroy($im);
    
    unlink($filename)
?>
