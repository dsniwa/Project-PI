<?php
session_start();

$limit=$config->login_limit->user;

//if logout trigger called
if(isset($_GET['logout'])) {
    $_SESSION['NamaUser'] = '';
	$_SESSION['UIDUser'] = '';
	session_destroy();
	header('Location:  ' . $_SERVER['PHP_SELF']);
}

//something interesting, just playing with script :3
if(!isset($_SESSION['fail'])){
	$no = 0;
	$_SESSION['fail'] = 0;
}else{
	$no=$_SESSION['fail'];
}
$no++;

if($_SESSION['fail'] >= $limit){
	$enableCaptcha = true;
}else{
	$enableCaptcha = false;
}

//mulai script login	
if(isset($_POST['submit'])) {

	//cek username di isi
	if(empty($_POST['username'])) {
		$_SESSION['fail']= $no; //adding fail every failed attempt
		$_SESSION['fail']= $no; //adding fail every failed attempt
	};

	//cek password di isi
	if(empty($_POST['password'])){
		$_SESSION['fail']= $no; //adding fail every failed attempt
		$_SESSION['fail']= $no; //adding fail every failed attempt
	};
}

// Cek terakhir

// Tanpa captcha
if($enableCaptcha==false){
	if(isset($_POST['username']) and isset($_POST['password'])){
		$username = $_POST['username'];
		$password = $_POST['password'];

		$query=$mysqli->prepare('SELECT * FROM pengguna WHERE username = ? OR email = ?');
		$query->bind_param('ss', $username, $username);
		$query->execute();

		$result = $query->get_result();
		while($row = $result->fetch_array()){
			$uid = $row['pengguna_id'];
			$hash = $row['hash'];
			$mailStatus = $row['email_status'];
			$lastLogin = $row['login_terakhir'];
		}

		$row = $result->num_rows;
		
		$query->close();

		if($row == 1){
			if(password_verify($password, $hash)){
				$loginStatus = true;
				$date = date("Y-m-d");
				$tanggal = formatIndonesia($date, true);
				$waktu = date("H:i:s");
				$loginStamp = $tanggal." | ".$waktu;
				$loginStampQuery=$mysqli->query("UPDATE pengguna SET login_terakhir='$loginStamp' WHERE username='$username'");
			}else{
				$loginStatus = false;
			}
		}else{
			$loginStatus = false;
		}

		if($loginStatus == true){
			$_SESSION['fail']= '';
			$_SESSION['NamaUser'] = $username;
			$_SESSION['UIDUser'] = $uid;
			$_SESSION['last_loginUser'] = $lastLogin;
		}else{
			$_SESSION['fail']= $no; //adding fail every failed attempt
			header("Location: /ketuplak/?fail");
		}
	}
// Dengan captcha
}else{
	if(isset($_POST['g-recaptcha-response'])){
		$captcha=$_POST['g-recaptcha-response'];
        if(!$captcha){
        	echo '<script>alert("Please check the the captcha form!");</script>';
    	}else{
    		$key = $config->api_key->googleCaptchaSecret;
			$serv = $_SERVER['REMOTE_ADDR'];
    		$response=json_decode(file_get_contents("https://www.google.com/recaptcha/api/siteverify?secret=$key&response=$captcha&remoteip=$serv"), true);
    		if($response['success'] == false){
        		$_SESSION['fail']= $no; //adding fail every failed attempt
				echo '<script>alert("Detected as Bot!");</script>';
				echo '<script>alert("Try again!");</script>';
    		}else{
        		if(isset($_POST['username']) and isset($_POST['password'])){
					$username = $_POST['username'];
					$password = $_POST['password'];

					$query=$mysqli->prepare('SELECT * FROM pengguna WHERE username = ? OR email = ?');
					$query->bind_param('ss', $username, $username);
					$query->execute();

					$result = $query->get_result();
					while($row = $result->fetch_array()){
						$uid = $row['pengguna_id'];
						$hash = $row['hash'];
						$mailStatus = $row['email_status'];
						$lastLogin = $row['login_terakhir'];
					}

					$row = $result->num_rows;
					
					$query->close();

					if($row == 1){
						if(password_verify($password, $hash)){
							$loginStatus = true;
							$date = date("Y-m-d");
							$tanggal = formatIndonesia($date, true);
							$waktu = date("H:i:s");
							$loginStamp = $tanggal." | ".$waktu;
							$loginStampQuery=$mysqli->query("UPDATE pengguna SET login_terakhir='$loginStamp' WHERE username='$username'");
						}else{
							$loginStatus = false;
						}
					}else{
						$loginStatus = false;
					}

					if($loginStatus == true){
						$_SESSION['fail']= '';
						$_SESSION['NamaUser'] = $username;
						$_SESSION['UIDUser'] = $uid;
						$_SESSION['last_loginUser'] = $lastLogin;
					}else{
						$_SESSION['fail']= $no; //adding fail every failed attempt
						header("Location: /ketuplak/?fail");
					}
				}
    		}
    	}
	}
}
?>
