<?php
if(isset($_SESSION['UIDSuperAdmin'])){
	$idSuperAdmin = $_SESSION['UIDSuperAdmin'];
	// Simpan variabel admin
	$query = $mysqli->prepare('SELECT * FROM superadmin WHERE BINARY super_id=?');
	$query->bind_param('s', $idSuperAdmin);
	$query->execute();

	$result = $query->get_result();
	while($c = $result->fetch_array()){
		$superadminID = $c['super_id'];
		$superadminUsername = $c['username'];
		$superadminEmail = $c['email'];
		$superadminNama = $c['nama'];
	}
	$query->close();
	if(!isset($superadminID)){
		unset($_SESSION['UIDSuperAdmin']);
	}
}

if(isset($_SESSION['UIDAdmin'])){
	$idAdmin = $_SESSION['UIDAdmin'];
	// Simpan variabel admin
	$query = $mysqli->prepare('SELECT * FROM admin WHERE BINARY admin_id=?');
	$query->bind_param('s', $idAdmin);
	$query->execute();

	$result = $query->get_result();
	while($c = $result->fetch_array()){
		$adminID = $c['admin_id'];
		$adminUsername = $c['username'];
		$adminEmail = $c['email'];
		$adminNama = $c['nama'];
		$adminStatus = $c['email_status'];
	}
	$query->close();
	if(!isset($adminID)){
		unset($_SESSION['UIDAdmin']);
	}
}

if(isset($_SESSION['UIDUser'])){
	$idPengguna = $_SESSION['UIDUser'];
	// Simpan variabel admin
	$query = $mysqli->prepare('SELECT * FROM pengguna WHERE BINARY pengguna_id=?');
	$query->bind_param('s', $idPengguna);
	$query->execute();

	$result = $query->get_result();
	while($c = $result->fetch_array()){
		$userID = $c['pengguna_id'];
		$userUsername = $c['username'];
		$userEmail = $c['email'];
		$userNama = $c['nama'];
		$userStatus = $c['email_status'];
	}
	$query->close();
	if(!isset($userID)){
		unset($_SESSION['UIDUser']);
	}
}
?>
