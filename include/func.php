<?php
class DBController {
	function runQuery($query) {
		global $config;
		$conn = mysqli_connect($config->db->host,$config->db->user,$config->db->pass,$config->db->name);
		$run = mysqli_query($conn,$query);
	}

	function selectQuery($query) {
		global $config;
		$conn = mysqli_connect($config->db->host,$config->db->user,$config->db->pass,$config->db->name);
		$result = mysqli_query($conn,$query);
		while($row=mysqli_fetch_assoc($result)){
			$resultset[] = $row;
		}
		if(!empty($resultset)){
			return $resultset;
		}else{
			return 0;
		}
	}

	function countQuery($query) {
		global $config;
		$conn = mysqli_connect($config->db->host,$config->db->user,$config->db->pass,$config->db->name);
		$result = mysqli_query($conn,$query);
		$rows = mysqli_fetch_row($result);
		return $rows[0];
	}
}

// Fungsi buat random key untuk ID unik berdasarkan microtime dan random string
function randomKey(){
	$characters = 'abcdefghijklmnopqrstuvwxyz0123456789';
	$string = '';
	$max = strlen($characters) - 1;
	for ($i = 0; $i < 3; $i++) {
		$string .= $characters[mt_rand(0, $max)];
	}
	$key = uniqid($string);
	return $key;
}

// Fungsi Format tangga ke format Indonesia ("Y-m-d")
function formatIndonesia($tanggal, $cetak_hari = false)
{
	$hari = array( 1 => 'Senin', 'Selasa', 'Rabu', 'Kamis', 'Jumat', 'Sabtu', 'Minggu' );
	$bulan = array(1 => 'Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember' );
	$split 	  = explode('-', $tanggal);
	$tgl_indo = $split[2] . ' ' . $bulan[ (int)$split[1] ] . ' ' . $split[0];
	
	if ($cetak_hari) {
		$num = date('N', strtotime($tanggal));
		return $hari[$num] . ', ' . $tgl_indo;
	}
	return $tgl_indo;
}

// Fungsi cek status acara
function statusAcara($acara_id){
	// Ambil data yang dibutuhkan
	$db_handle = new DBController();
	$query ="SELECT status,kuota FROM acara WHERE acara_id='$acara_id'";
    $results = $db_handle->selectQuery($query);
    foreach($results as $acara) {
        $status = $acara["status"];
        $kuota = $acara["kuota"];
    }

	if($status == 2){
		// Jika sudah di tutup skip perintah di bawah
		$status = 2;
	}else{
		// Hitung jumlah peserta
		$db_handle = new DBController();
        $query ="SELECT COUNT(id) FROM `peserta` WHERE acara_id = '$acara_id'";
        $jumlah = $db_handle->countQuery($query);

        // Jika sudah mencapai kuota, tutup acara
        if($jumlah >= $kuota){
        	$db_handle = new DBController();
			$query ="UPDATE acara SET status ='2' WHERE acara_id = '$acara_id'";
		    $results = $db_handle->runQuery($query);
		    $status = 2;
        }else{
        	$status = 1;
        }
	}
	return $status;
}

// Fungsi cek status acara
function statusAcaraEdit($idAcara,$kuotaAcara){
	// Hitung jumlah peserta
	$db_handle = new DBController();
	$query ="SELECT COUNT(id) FROM `peserta` WHERE acara_id = '$idAcara'";
	$jumlah = $db_handle->countQuery($query);

	if($jumlah >= $kuotaAcara){
		// Jika sudah mencapai kuota, tutup acara
		$query ="UPDATE acara SET status ='2' WHERE acara_id = '$idAcara'";
		$results = $db_handle->runQuery($query);
	}else{
		$query ="UPDATE acara SET status ='1' WHERE acara_id = '$idAcara'";
		$results = $db_handle->runQuery($query);
	}
}
?>
