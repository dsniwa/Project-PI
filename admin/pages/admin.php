<!-- Head -->
<?php include "head.php"; ?>
    
    <!-- Navigation BAR -->
    <?php include "navbar.php"; ?>

    <section class="content">
        <div class="container-fluid">

            <?php if(isset($regStatus)){
                if($regStatus=="sukses"){
            ?>
                <div class="alert bg-green alert-dismissible" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                    BEM baru <strong style="color: blue;"><?php echo $regID; ?></strong> berhasil ditambahkan. Ketua BEM akan menerima email mengenai akunnya.
                </div>
            <?php }else{ ?>
                <div class="alert bg-red alert-dismissible" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                <?php if($regStatMess==""||$regStatMess==null){$regStatMess="Unknown";} ?>
                    BEM gagal ditambahkan, silahkan coba beberapa saat lagi. Pesan error: <?php echo $regStatMess; ?>
                </div>
            <?php } } ?>

            <?php if(isset($statusGanti)){
                if($statusGanti==1){
            ?>
                <div class="alert bg-green alert-dismissible" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                    Data berhasil diubah.<?php if($adminChMail==true){ ?> Link konfirmasi telah dikirim ke email baru Ketua BEM tersebut.<?php } ?>
                </div>
            <?php }else{ ?>
                <div class="alert bg-red alert-dismissible" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                    Data gagal diubah, silahkan coba beberapa saat lagi. Pesan error: Email telah digunakan pada akun lain atau pada akun tersebut.
                </div>
            <?php } } ?>

            <!-- Exportable Table -->
            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <h2>TAMBAH BEM</h2>
                            <ul class="header-dropdown m-r--5" style="top: 15px !important;">
                                <button type="button" class="toggle-btn btn bg-deep-purple waves-effect" style="float: right;"><sapn>Tampilkan</sapn></button>
                            </ul>
                        </div>
                        <div class="body umpet" style="display: none">
                            <div class="row clearfix">
                                <div class="col-sm-12">
                                <form id="form_validation" method="POST">
                                    <div class="form-group form-float">
                                        <div class="form-line">
                                            <input type="text" class="form-control" name="newUsername" required />
                                            <label class="form-label">Username</label>
                                        </div>
                                    </div>
                                    
                                    <div class="row clearfix">
                                    <div class="col-sm-10" style="margin-bottom: 0px !important;">
                                        <div class="form-group form-float">
                                            <div class="form-line">
                                                <input type="text" class="form-control" name="newPassword" required readonly />
                                                <label class="form-label">Password</label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-2" style="margin-bottom: 0px !important;">
                                        <div class="form-group">
                                            <a class="btn btn-primary m-t-15 waves-effect gen-pw" style="width:100%;">GENERATE</a>
                                        </div>
                                    </div>
                                    </div>

                                    <div class="form-group form-float">
                                        <div class="form-line">
                                            <input type="text" class="form-control" name="newName" required />
                                            <label class="form-label">Nama BEM</label>
                                        </div>
                                    </div>
                                    <div class="form-group form-float">
                                        <div class="form-line">
                                            <input type="text" class="form-control" name="newEmail" required />
                                            <label class="form-label">Email</label>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <input type="submit" name="newRegister" class="btn btn-primary m-t-15 waves-effect" />
                                    </div>
                                </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- #END# Exportable Table -->

            <!-- Exportable Table -->
            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <h2>DAFTAR BEM</h2>
                        </div>
                        <div class="body">
                            <table style="width: 100%;" class="table table-bordered table-striped table-hover dataTable table-basic">
                                <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>Username</th>
                                        <th>Nama BEM</th>
                                        <th>Email</th>
                                        <th>Waktu Terdaftar</th>
                                        <th>Waktu Login Terakhir</th>
                                        <th>Status</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                <?php
                                    $query=$mysqli->prepare('SELECT * FROM admin');
                                    $query->execute();
                                    $result=$query->get_result();

                                    $no = 0;
                                    while($row = $result->fetch_array()){
                                        $no++;
                                        $kodeStat = $row['email_status'];
                                        if($kodeStat == 1){
                                            $status = "Aktif";
                                        }else{
                                            $status = "Tidak Aktif";
                                        }

                                        echo '
                                            <tr>
                                                <td>'.$no.'</td>
                                                <td>'.$row['username'].'</td>
                                                <td>'.$row['nama'].'</td>
                                                <td>'.$row['email'].'</td>
                                                <td>'.$row['waktu_daftar'].'</td>
                                                <td>'.$row['login_terakhir'].'</td>
                                                <td>'.$status.'</td>
                                                <td class="middle" style="text-align:center;" nowrap>
                                                    <a href="edit/'.$row['admin_id'].'" title="Edit" class="btn btn-xs bg-blue waves-effect"><i class="material-icons">edit</i></a>
                                                    <a href="javascript:void(0)" data-value="'.$row['admin_id'].'" onclick="admDel(\''.$row['admin_id'].'\');" title="Delete" class="btn btn-danger btn-xs waves-effect"><i class="material-icons">delete</i></a><a href="javascript:void(0)" id="dt-del-btn-b"></a>
                                                </td>
                                            </tr>
                                        ';
                                    }
                                    ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <!-- #END# Exportable Table -->

        </div>
    </section>
    
    <!-- Jquery Core Js -->
    <script src="/dash/plugins/jquery/jquery.min.js"></script>

    <!-- Bootstrap Core Js -->
    <script src="/dash/plugins/bootstrap/js/bootstrap.js"></script>

    <!-- Slimscroll Plugin Js -->
    <script src="/dash/plugins/jquery-slimscroll/jquery.slimscroll.js"></script>

    <!-- Waves Effect Plugin Js -->
    <script src="/dash/plugins/node-waves/waves.js"></script>

    <!-- Jquery Validation Plugin Js -->
    <script src="/dash/plugins/jquery-validation/jquery.validate.js"></script>
    <script src="/dash/plugins/jquery-validation/localization/messages_id.js"></script>

    <!-- Jquery DataTable Plugin Js -->
    <script src="/dash/plugins/jquery-datatable/jquery.dataTables.js"></script>
    <script src="/dash/plugins/jquery-datatable/skin/bootstrap/js/dataTables.bootstrap.js"></script>
    <script src="/dash/plugins/jquery-datatable/extensions/export/dataTables.buttons.min.js"></script>
    <script src="/dash/plugins/jquery-datatable/extensions/export/buttons.flash.min.js"></script>
    <script src="/dash/plugins/jquery-datatable/extensions/export/jszip.min.js"></script>
    <script src="/dash/plugins/jquery-datatable/extensions/export/pdfmake.min.js"></script>
    <script src="/dash/plugins/jquery-datatable/extensions/export/vfs_fonts.js"></script>
    <script src="/dash/plugins/jquery-datatable/extensions/export/buttons.html5.min.js"></script>
    <script src="/dash/plugins/jquery-datatable/extensions/export/buttons.print.min.js"></script>

    <script src="https://cdn.datatables.net/fixedcolumns/3.2.2/js/dataTables.fixedColumns.min.js"></script>

    <!-- Select Plugin Js -->
    <script src="/dash/plugins/bootstrap-select/js/bootstrap-select.js"></script>

    <!-- Autosize Plugin Js -->
    <script src="/dash/plugins/autosize/autosize.js"></script>

    <!-- Moment Plugin Js -->
    <script src="/dash/plugins/momentjs/moment.js"></script>

    <!-- Bootstrap Material Datetime Picker Plugin Js -->
    <script src="/dash/plugins/bootstrap-material-datetimepicker/js/bootstrap-material-datetimepicker.js"></script>

    <!-- SweetAlert Plugin Js -->
    <script src="/dash/plugins/sweetalert/sweetalert.min.js"></script>

    <!-- Custom Js -->
    <script src="/dash/js/admin.js"></script>
    <script src="/dash/js/jquery-datatable.js"></script>
    <script src="/dash/js/ajax-func.js"></script>
    <script src="/dash/js/basic-form-elements.js"></script>
    <script src="/dash/js/form-validation.js"></script>
</body>

</html>
