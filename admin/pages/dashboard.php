﻿<!-- Head -->
<?php include "head.php"; ?>
    
    <!-- Navigation BAR -->
    <?php include "navbar.php"; ?>

    <section class="content">
        <div class="container-fluid">
            <div class="block-header">
                <h2>DASHBOARD</h2>
            </div>

            <!-- Widgets -->
            <div class="row clearfix">
                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                    <div class="info-box bg-light-green hover-expand-effect">
                        <div class="icon">
                            <i class="material-icons">group</i>
                        </div>
                        <div class="content">
                            <div class="text">BEM</div>
                            <?php
                            $db_handle = new DBController();
                            $query ="SELECT COUNT(id) FROM `admin`";
                            $results = $db_handle->countQuery($query);
                            ?>
                            <div class="number count-to" data-from="0" data-to="<?php echo $results; ?>" data-speed="15" data-fresh-interval="20"></div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                    <div class="info-box bg-orange hover-expand-effect">
                        <div class="icon">
                            <i class="material-icons">forum</i>
                        </div>
                        <div class="content">
                            <div class="text">PESAN</div>
                            <?php
                            $db_handle = new DBController();
                            $query ="SELECT COUNT(id_pesan) FROM `pesan`";
                            $results = $db_handle->countQuery($query);
                            ?>
                            <div class="number count-to" data-from="0" data-to="<?php echo $results; ?>" data-speed="125" data-fresh-interval="20"></div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row clearfix">
                <!-- Task Info -->
                <style>
                    .modal{
                        z-index:9000;
                        background: rgba( 255, 255, 255, 0 );
                    }
                </style>
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <div class="card">
                        <div class="header">
                            <h2>PESAN TERBARU</h2>
                        </div>
                        <div class="body">
                            <div class="table-responsive">
                                <table class="table table-hover dashboard-task-infos">
                                    <thead>
                                        <tr>
                                            <th class="middle">#</th>
                                            <th class="middle">Pengirim</th>
                                            <th class="middle">Email</th>
                                            <th class="middle">Pesan</th>
                                            <th class="middle">Status</th>
                                            <th class="middle">Tindakan</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php
                                        $query=$mysqli->prepare('SELECT * FROM pesan ORDER BY id_pesan DESC LIMIT 5');
                                        $query->execute();
                                        $result=$query->get_result();
                                        $no = 0;
                                        while($row = $result->fetch_array()){
                                            $no++;
                                            
                                            if($row['status']==0){
                                                $status = "<span class=\"label bg-green\">Open</span>";
                                            }else{
                                                $status = "<span class=\"label bg-red\">Closed</span>";
                                            }

                                            $pesanFull = $row['pesan'];
                                            if (strlen($row['pesan']) > 100) {
                                                $pesan = substr($row['pesan'], 0, 100).".....";
                                            } else {
                                                $pesan = $row['pesan'];
                                            }
                                            echo '
                                            <tr>
                                                <td class="middle">'.$no.'</td>
                                                <td class="middle">'.$row['nama'].'</td>
                                                <td class="middle">'.$row['email'].'</td>
                                                <td class="middle">'.$pesan.'</td>
                                                <td class="middle">'.$status.'</td>
                                                <td class="middle">
                                                    <a class="btn btn-default waves-effect m-r-20" data-toggle="modal" data-target="#modal'.$no.'">View</a>
                                                    <div class="modal fade" id="modal'.$no.'" tabindex="-1" role="dialog">
                                                        <div class="modal-dialog" role="document">
                                                            <div class="modal-content">
                                                                <div class="modal-header">
                                                                    <h4 class="modal-title" id="modal'.$no.'Label">'.$row['nama'].' - '.$row['email'].'</h4>
                                                                </div>
                                                                <div class="modal-body">
                                                                    '.$pesanFull.'
                                                                </div>
                                                                <div class="modal-footer">
                                                                    <button type="button" class="btn btn-link waves-effect" data-dismiss="modal">CLOSE</button>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </td>
                                            </tr>';
                                        }
                                        ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- #END# Task Info -->
            </div>
        </div>
    </section>
    
    <!-- Jquery Core Js -->
    <script src="/dash/plugins/jquery/jquery.min.js"></script>

    <!-- Bootstrap Core Js -->
    <script src="/dash/plugins/bootstrap/js/bootstrap.js"></script>

    <!-- Slimscroll Plugin Js -->
    <script src="/dash/plugins/jquery-slimscroll/jquery.slimscroll.js"></script>

    <!-- Waves Effect Plugin Js -->
    <script src="/dash/plugins/node-waves/waves.js"></script>

    <!-- Jquery CountTo Plugin Js -->
    <script src="/dash/plugins/jquery-countto/jquery.countTo.js"></script>

    <!-- Autosize Plugin Js -->
    <script src="/dash/plugins/autosize/autosize.js"></script>

    <!-- Custom Js -->
    <script src="/dash/js/admin.js"></script>
    <script src="/dash/js/index.js"></script>
</body>

</html>
