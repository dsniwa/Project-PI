<!-- Head -->
<?php include "head.php"; ?>
    
    <!-- Navigation BAR -->
    <?php include "navbar.php"; ?>

    <section class="content">
        <div class="container-fluid">
            <!-- Exportable Table -->
            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <h2>EDIT DATA BEM</h2>
                        </div>
                        <div class="body">
                            <div class="row clearfix">
                                <div class="col-sm-12">
                                <form method="POST" action="/admin/bem/">
                                    <?php
                                    $filter = $_GET['y'];
                                    $query=$mysqli->prepare('SELECT * FROM admin WHERE admin_id = ?');
                                    $query->bind_param('s', $filter);
                                    $query->execute();
                                    $result=$query->get_result();

                                    while($row = $result->fetch_array()){
                                        $username = $row['username'];
                                        $nama = $row['nama'];
                                        $email = $row['email'];
                                    }
                                    ?>
                                    <div class="form-group form-float">
                                        <div class="form-line">
                                            <input type="hidden" name="adminID" value="<?php echo $filter; ?>">
                                            <input type="text" class="form-control" value="<?php echo $username; ?>" disabled/>
                                            <label class="form-label">Username</label>
                                        </div>
                                    </div>
                                    <div class="form-group form-float">
                                        <div class="form-line">
                                            <input type="text" class="form-control" name="nama" value="<?php echo $nama; ?>" required />
                                            <label class="form-label">Nama BEM</label>
                                        </div>
                                    </div>
                                    <div class="form-group form-float">
                                        <div class="form-line">
                                            <input type="text" class="form-control" name="email" value="<?php echo $email; ?>" required />
                                            <label class="form-label">Email</label>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <input type="submit" name="editAdmin" value="Ubah Data" class="btn btn-block btn-primary m-t-15 waves-effect" />
                                    </div>
                                </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- #END# Exportable Table -->
        </div>
    </section>
    
    <!-- Jquery Core Js -->
    <script src="/dash/plugins/jquery/jquery.min.js"></script>

    <!-- Bootstrap Core Js -->
    <script src="/dash/plugins/bootstrap/js/bootstrap.js"></script>

    <!-- Slimscroll Plugin Js -->
    <script src="/dash/plugins/jquery-slimscroll/jquery.slimscroll.js"></script>

    <!-- Waves Effect Plugin Js -->
    <script src="/dash/plugins/node-waves/waves.js"></script>

    <!-- Jquery DataTable Plugin Js -->
    <script src="/dash/plugins/jquery-datatable/jquery.dataTables.js"></script>
    <script src="/dash/plugins/jquery-datatable/skin/bootstrap/js/dataTables.bootstrap.js"></script>
    <script src="/dash/plugins/jquery-datatable/extensions/export/dataTables.buttons.min.js"></script>
    <script src="/dash/plugins/jquery-datatable/extensions/export/buttons.flash.min.js"></script>
    <script src="/dash/plugins/jquery-datatable/extensions/export/jszip.min.js"></script>
    <script src="/dash/plugins/jquery-datatable/extensions/export/pdfmake.min.js"></script>
    <script src="/dash/plugins/jquery-datatable/extensions/export/vfs_fonts.js"></script>
    <script src="/dash/plugins/jquery-datatable/extensions/export/buttons.html5.min.js"></script>
    <script src="/dash/plugins/jquery-datatable/extensions/export/buttons.print.min.js"></script>

    <script src="https://cdn.datatables.net/fixedcolumns/3.2.2/js/dataTables.fixedColumns.min.js"></script>

    <!-- Select Plugin Js -->
    <script src="/dash/plugins/bootstrap-select/js/bootstrap-select.js"></script>

    <!-- Autosize Plugin Js -->
    <script src="/dash/plugins/autosize/autosize.js"></script>

    <!-- Moment Plugin Js -->
    <script src="/dash/plugins/momentjs/moment.js"></script>

    <!-- Bootstrap Material Datetime Picker Plugin Js -->
    <script src="/dash/plugins/bootstrap-material-datetimepicker/js/bootstrap-material-datetimepicker.js"></script>

    <!-- Custom Js -->
    <script src="/dash/js/admin.js"></script>
    <script src="/dash/js/jquery-datatable.js"></script>
    <script src="/dash/js/basic-form-elements.js"></script>
</body>

</html>
