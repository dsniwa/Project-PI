    <section>
        <!-- Left Sidebar -->
        <aside id="leftsidebar" class="sidebar">
            <!-- User Info -->
            <div class="user-info">
                <div class="image">
                    <img src="/admin/images/user-photos/default-user.png" width="48" height="48" alt="User" />
                </div>
                <div class="info-container">
                    <div class="name" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><?php echo $superadminNama ?></div>
                    <div class="email"><?php echo $superadminEmail ?></div>
                    <div class="btn-group user-helper-dropdown">
                        <i class="material-icons" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">keyboard_arrow_down</i>
                        <ul class="dropdown-menu pull-right">
                            <li><a href="?logout"><i class="material-icons">input</i>Sign Out</a></li>
                        </ul>
                    </div>
                </div>
            </div>
            <!-- #User Info -->
            <!-- Menu -->
            <div class="menu">
                <ul class="list">
                    <li class="header">NAVIGASI</li>
                    <li <?php if($_GET['page']=="dashboard"){echo 'class="active"';} ?>>
                        <a href="/admin/dashboard/">
                            <i class="material-icons">dashboard</i>
                            <span>Dashboard</span>
                        </a>
                    </li>
                    <li <?php if($_GET['page']=="bem"){echo 'class="active"';} ?>>
                        <a href="/admin/bem/">
                            <i class="material-icons">group</i>
                            <span>BEM</span>
                        </a>
                    </li>
                    <li <?php if($_GET['page']=="pesan"){echo 'class="active"';} ?>>
                        <a href="/admin/pesan/">
                            <i class="material-icons">message</i>
                            <span>Pesan</span>
                        </a>
                    </li>
                    <!--li>
                        <a href="javascript:void(0);" class="menu-toggle">
                            <i class="material-icons">widgets</i>
                            <span>Multi Level</span>
                        </a>
                        <ul class="ml-menu">
                            <li>
                                <a href="javascript:void(0);">
                                    <span>List 1</span>
                                </a>
                            </li>
                            <li>
                                <a href="javascript:void(0);">
                                    <span>List 2</span>
                                </a>
                            </li>
                        </ul>
                    </li-->
                </ul>
            </div>
            <!-- #Menu -->
        </aside>
        <!-- #END# Left Sidebar -->
    </section>
