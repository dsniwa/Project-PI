<!-- Head -->
<?php include "head.php"; ?>
    
    <!-- Navigation BAR -->
    <?php include "navbar.php"; ?>

    <section class="content">
        <div class="container-fluid">

            <?php if(isset($statusGanti)){
                if($statusGanti==1){
            ?>
                <div class="alert bg-green alert-dismissible" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                    Status berhasil diubah.
                </div>
            <?php }else{ ?>
                <div class="alert bg-red alert-dismissible" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                    Status gagal diubah.
                </div>
            <?php } } ?>

            <!-- Exportable Table -->
            <div class="row clearfix">
                <style>
                    .modal{
                        z-index:9000;
                        background: rgba( 255, 255, 255, 0 );
                    }
                </style>
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <h2>DAFTAR PESAN</h2>
                        </div>
                        <div class="body">
                            <table style="width: 100%;" class="table table-bordered table-striped table-hover dataTable table-basic">
                                <thead>
                                    <tr>
                                        <th class="middle">#</th>
                                        <th class="middle">Pengirim</th>
                                        <th class="middle">Email</th>
                                        <th class="middle">Pesan</th>
                                        <th class="middle">Status</th>
                                        <th class="middle">Tindakan</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    $query=$mysqli->prepare('SELECT * FROM pesan ORDER BY id_pesan DESC');
                                    $query->execute();
                                    $result=$query->get_result();
                                    $no = 0;
                                    while($row = $result->fetch_array()){
                                        $no++;
                                        
                                        if($row['status']==0){
                                            $status = "<span class=\"label bg-green\">Open</span>";
                                            $button = "<a href=\"/admin/pesan/close/".$row['id_pesan']."\" class=\"btn btn-danger btn-xs waves-effect\"><i class=\"material-icons\">lock_outline</i></a>";
                                        }else{
                                            $status = "<span class=\"label bg-red\">Closed</span>";
                                            $button = "<a href=\"javascript:void(0)\" style=\"cursor: not-allowed; color: gray;\"  title=\"Disabled\" class=\"btn bg-grey btn-xs waves-effect\"><i class=\"material-icons\">lock_open</i></a>";
                                        }

                                        $pesanFull = $row['pesan'];
                                        if (strlen($row['pesan']) > 100) {
                                            $pesan = substr($row['pesan'], 0, 100).".....";
                                        } else {
                                            $pesan = $row['pesan'];
                                        }
                                        echo '
                                        <tr>
                                            <td class="middle">'.$no.'</td>
                                            <td class="middle">'.$row['nama'].'</td>
                                            <td class="middle"><a href="mailto:'.$row['email'].'">'.$row['email'].'</a></td>
                                            <td class="middle">'.$pesan.'</td>
                                            <td class="middle">'.$status.'</td>
                                            <td class="middle">
                                                <a class="btn btn-xs bg-blue waves-effect" data-toggle="modal" data-target="#modal'.$no.'"><i class="material-icons">remove_red_eye</i></a>
                                                '.$button.'
                                                <div class="modal fade" id="modal'.$no.'" tabindex="-1" role="dialog">
                                                    <div class="modal-dialog" role="document">
                                                        <div class="modal-content">
                                                            <div class="modal-header">
                                                                <h4 class="modal-title" id="modal'.$no.'Label">'.$row['nama'].' - '.$row['email'].'</h4>
                                                            </div>
                                                            <div class="modal-body">
                                                                '.$pesanFull.'
                                                            </div>
                                                            <div class="modal-footer">     
                                                                <button type="button" class="btn btn-link waves-effect" data-dismiss="modal">CLOSE</button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>';
                                    }
                                    ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <!-- #END# Exportable Table -->

        </div>
    </section>
    
    <!-- Jquery Core Js -->
    <script src="/dash/plugins/jquery/jquery.min.js"></script>

    <!-- Bootstrap Core Js -->
    <script src="/dash/plugins/bootstrap/js/bootstrap.js"></script>

    <!-- Slimscroll Plugin Js -->
    <script src="/dash/plugins/jquery-slimscroll/jquery.slimscroll.js"></script>

    <!-- Waves Effect Plugin Js -->
    <script src="/dash/plugins/node-waves/waves.js"></script>

    <!-- Jquery Validation Plugin Js -->
    <script src="/dash/plugins/jquery-validation/jquery.validate.js"></script>
    <script src="/dash/plugins/jquery-validation/localization/messages_id.js"></script>

    <!-- Jquery DataTable Plugin Js -->
    <script src="/dash/plugins/jquery-datatable/jquery.dataTables.js"></script>
    <script src="/dash/plugins/jquery-datatable/skin/bootstrap/js/dataTables.bootstrap.js"></script>
    <script src="/dash/plugins/jquery-datatable/extensions/export/dataTables.buttons.min.js"></script>
    <script src="/dash/plugins/jquery-datatable/extensions/export/buttons.flash.min.js"></script>
    <script src="/dash/plugins/jquery-datatable/extensions/export/jszip.min.js"></script>
    <script src="/dash/plugins/jquery-datatable/extensions/export/pdfmake.min.js"></script>
    <script src="/dash/plugins/jquery-datatable/extensions/export/vfs_fonts.js"></script>
    <script src="/dash/plugins/jquery-datatable/extensions/export/buttons.html5.min.js"></script>
    <script src="/dash/plugins/jquery-datatable/extensions/export/buttons.print.min.js"></script>

    <script src="https://cdn.datatables.net/fixedcolumns/3.2.2/js/dataTables.fixedColumns.min.js"></script>

    <!-- Select Plugin Js -->
    <script src="/dash/plugins/bootstrap-select/js/bootstrap-select.js"></script>

    <!-- Autosize Plugin Js -->
    <script src="/dash/plugins/autosize/autosize.js"></script>

    <!-- Moment Plugin Js -->
    <script src="/dash/plugins/momentjs/moment.js"></script>

    <!-- Bootstrap Material Datetime Picker Plugin Js -->
    <script src="/dash/plugins/bootstrap-material-datetimepicker/js/bootstrap-material-datetimepicker.js"></script>

    <!-- SweetAlert Plugin Js -->
    <script src="/dash/plugins/sweetalert/sweetalert.min.js"></script>

    <!-- Custom Js -->
    <script src="/dash/js/admin.js"></script>
    <script src="/dash/js/jquery-datatable.js"></script>
    <script src="/dash/js/ajax-func.js"></script>
    <script src="/dash/js/basic-form-elements.js"></script>
    <script src="/dash/js/form-validation.js"></script>
</body>

</html>
