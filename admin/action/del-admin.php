<?php
// sertakan berkas utama
$role = "superadmin";
require_once $_SERVER['DOCUMENT_ROOT'].'/include/load.php';
$db_handle = new DBController();

// Cek status login
if(empty($_SESSION['UIDSuperAdmin'])){
    header("HTTP/1.1 404 Not Found");
    die();
}

// String
$adminID = $_POST['id'];

// Delete acara, post, dan peserta
$query=$mysqli->prepare('DELETE pengguna, acara, posting, peserta, admin FROM pengguna, acara, posting, peserta, admin WHERE admin.admin_id=pengguna.admin_id OR pengguna.pengguna_id=acara.pengguna_id OR pengguna.pengguna_id=posting.pengguna_id OR pengguna.pengguna_id=peserta.pengguna_id OR admin.admin_id = ? OR pengguna.admin_id = ?');
$query->bind_param('ss', $adminID, $adminID);
if($query->execute()){
    $response_array['status'] = 'success';
}else{
    $response_array['status'] = 'failed';
}
$query->close();

header('Content-type: application/json');
echo json_encode($response_array);
?>
