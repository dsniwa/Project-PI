<?php
// Cek status login
if(empty($_SESSION['UIDSuperAdmin'])){
    header("HTTP/1.1 404 Not Found");
    die();
}

if(isset($_POST['newRegister'])){
    // Default
    $regFail = false;

    // Waktu daftar
    $now = date('Y-m-d');
    $today = new DateTime($now);
    $regtimeTmp = $today->format('Y-m-d');
    
    // Hash password
    $password = $_POST['newPassword'];
    $options = [
    'cost' => 10
    ];
    $hashTmp = password_hash($password, PASSWORD_BCRYPT, $options);

    // Ambil variabel
    $UID = randomKey();
    $username = $_POST['newUsername'];
    $hash = $hashTmp;
    $nama = $_POST['newName'];
    $email = $_POST['newEmail'];
    $regtime = $regtimeTmp;
    $email_hash = hash('sha512', $UID);

    // Check username
    $query1 = $mysqli->prepare('SELECT * FROM admin WHERE username = ?');
    $query1->bind_param('s', $username);
    $query1->execute();
    $result1=$query1->get_result();
    $jumlahBaris1=$result1->num_rows;
    if($jumlahBaris1 > 0){
        $regFail = true;
        $regStatMess = "Username";
    }

    // Check email
    $query2 = $mysqli->prepare('SELECT * FROM admin WHERE email = ? OR ganti_email = ?');
    $query2->bind_param('ss', $email, $email);
    $query2->execute();
    $result2=$query2->get_result();
    $jumlahBaris2=$result2->num_rows;
    if($jumlahBaris2 > 0){
        $regFail = true;
        if(isset($regStatMess)){
            $regStatMess = "Username & Email";
        }else{
            $regStatMess = "Email";
        }
    }

    if($regFail == false){
        // Insert to table and get id
        $query = $mysqli->prepare('INSERT INTO admin (admin_id, username, hash, nama, email, waktu_daftar, email_hash)values(?, ?, ?, ?, ?, ?, ?)');
        $query->bind_param('sssssss', $UID, $username, $hash, $nama, $email, $regtime, $email_hash);
        $query->execute();
        require_once "register.mail.php";
        $regStatus = "sukses";
        $regID = $username;
    }else{
        $regStatus = "gagal";
    }
}

?>
