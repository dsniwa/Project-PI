﻿<?php
// sertakan berkas utama
$role = "superadmin";
require_once $_SERVER['DOCUMENT_ROOT'].'/include/load.php';

if(empty($_SESSION['UIDSuperAdmin'])){
    if($enableCaptchaSuperAdmin==false){
        include 'pages/login.php';
    }else if($enableCaptchaSuperAdmin==true){
        include 'pages/login-captcha.php';
    }
die();
}

if(!isset($_GET['page'])){
    $page = "";
}else{
    $page = $_GET['page'];
}

if($page == "dashboard"){
	$pageTitle = "Dashboard | Panel Admin";
    include "pages/dashboard.php";

// Halaman manage admin
}else if($page == "bem"){

    if(isset($_GET['x'])){
        $x = $_GET['x'];
        if($x == "edit"){
            $pageTitle = "Daftar BEM | Panel Admin";
            include "pages/edit-admin.php";
            die;
        }
    }

    if(isset($_POST['editAdmin'])){
        include "action/edit-admin.php";
    }

	$pageTitle = "Daftar BEM | Panel Admin";
	// Tambah user
	if(isset($_POST['newRegister'])){
		include "action/add-admin.php";
	}
    include "pages/admin.php";

}else if($page == "pesan"){

    if(isset($_GET['x'])){
        if($_GET['x'] == "close"){
            if(isset($_GET['y'])){
                $idPesan = $_GET['y'];
                include "action/close-pesan.php";
            }else{
                header("Location: /superadmin/pesan/");
            }
        }
    }

	$pageTitle = "Daftar Pesan | Panel Admin";
    include "pages/pesan.php";

}else if($page == ""){
    header("Location: /admin/dashboard/");
}else{
    header("Location: /admin/dashboard/");
}
?>
