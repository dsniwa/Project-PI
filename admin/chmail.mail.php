<?php
require_once $_SERVER['DOCUMENT_ROOT'].'/lib/PHPMailer/PHPMailerAutoload.php';

function get_include_contents($filename, $variablesToMakeLocal) {
    extract($variablesToMakeLocal);
    if (is_file($filename)) {
        ob_start();
        include $filename;
    return ob_get_clean();
    }
    return false;
}

$data['judul'] = "Email Change";
$data['header'] = "Silahkan klik tombol pada email ini untuk konfirmasi email baru Anda.";
$data['one'] = "Halo $nama,";
$data['two'] = "Silahkan klik tombol di bawah untuk mengganti email Anda";
$data['link'] = $_SERVER['SERVER_NAME']."/bem/chmail.php?id=$admin_id&token=$mailHash";
$data['button'] = "Change Email";
$data['three'] = "Jika tombol di atas tidak dapat di klik, silahkan copy & paste link di bawah ini ke address bar browser Anda.<br/>".$_SERVER['SERVER_NAME']."/bem/chmail.php?id=$admin_id&token=$mailHash";
$data['four'] = "Jika Anda merasa tidak melakukan hal ini, silahkan abaikan email ini.";

$mail = new PHPMailer;

//$mail->SMTPDebug = 2;

$mail->isSMTP();// Set mailer to use SMTP
$mail->Host         = $config->mail->host;// Specify main and backup SMTP servers
$mail->SMTPAuth     = true;// Enable SMTP authentication
$mail->Username     = $config->mail->norepUser;// SMTP username
$mail->Password     = $config->mail->norepPass;// SMTP password
$mail->SMTPSecure   = 'tls';// Enable TLS encryption, `ssl` also accepted
$mail->Port         = 587;// TCP port to connect to

$mail->setFrom($config->mail->norepMail, 'KopiAdem noreply');
$mail->addAddress($email, $nama);// Add a recipient
//$mail->AddBCC('no-reply@events.kopiadem.com');// Send a copy to our server

$mail->isHTML(true);// Set email format to HTML

$mail->Subject = 'Verify Your Email Address';
$mail->Body    = get_include_contents($_SERVER['DOCUMENT_ROOT'].'/lib/email-template.php', $data); 
if(!$mail->send()) {
    echo 'Message could not be sent.<br/>';
    echo 'Mailer Error: ' . $mail->ErrorInfo;
}
?>
