﻿<!-- Head -->
<?php include "head.php"; ?>
    
    <!-- Navigation BAR -->
    <?php include "navbar.php"; ?>

    <section class="content">
        <div class="container-fluid">
            <?php
                $query=$mysqli->prepare('SELECT ganti_email FROM admin WHERE admin_id = ?');
                $query->bind_param('s', $adminID);
                $query->execute();
                $result=$query->get_result();
                while($row = $result->fetch_array()){
                    $ganti_email = $row['ganti_email'];
                }
                $query->close();
            ?>

            <?php if($ganti_email != ""){ ?>
                <div class="alert bg-orange alert-dismissible" role="alert">
                    Email <?php echo $ganti_email; ?> sedang menunggu konfirmasi. Jika link konfirmasi Anda hilang atau tidak menerima link konfirmasi pada email Anda, silahkan <a href="/admin/chresend.php" style="color:black;"> kirim ulang link konfirmasi</a>. Atau <a href="/admin/revoke.php" style="color:black;"> batalkan ganti email</a>.
                </div>
            <?php } ?>
            <div class="block-header">
                <div class="row clearfix">
                    <div class="col-sm-6"><h2>DASHBOARD</h2></div>
                    <div class="col-sm-6"><span style="float: right;">Terakhir Login: <?php echo $_SESSION['last_loginAdmin']; ?></span></div>
                </div>
            </div>

            <!-- Widgets -->
            <div class="row clearfix">
                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                    <div class="info-box bg-pink hover-expand-effect">
                        <div class="icon">
                            <i class="material-icons">group</i>
                        </div>
                        <div class="content">
                            <div class="text">KETUPLAK</div>
                            <?php
                            $db_handle = new DBController();
                            $query ="SELECT COUNT(id) FROM `pengguna` WHERE admin_id = '$adminID'";
                            $results = $db_handle->countQuery($query);
                            ?>
                            <div class="number count-to" data-from="0" data-to="<?php echo $results; ?>" data-speed="15" data-fresh-interval="20"></div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                    <div class="info-box bg-cyan hover-expand-effect">
                        <div class="icon">
                            <i class="material-icons">event</i>
                        </div>
                        <div class="content">
                            <div class="text">ACARA</div>
                            <?php
                            $db_handle = new DBController();
                            $query ="SELECT COUNT(acara.id) FROM `acara` INNER JOIN `pengguna` ON acara.pengguna_id=pengguna.pengguna_id WHERE pengguna.admin_id = '$adminID'";
                            $results = $db_handle->countQuery($query);
                            ?>
                            <div class="number count-to" data-from="0" data-to="<?php echo $results; ?>" data-speed="75" data-fresh-interval="20"></div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                    <div class="info-box bg-light-green hover-expand-effect">
                        <div class="icon">
                            <i class="material-icons">person_add</i>
                        </div>
                        <div class="content">
                            <div class="text">PESERTA</div>
                            <?php
                            $db_handle = new DBController();
                            $query ="SELECT COUNT(peserta.id) FROM `peserta` INNER JOIN `pengguna` ON pengguna.pengguna_id = peserta.pengguna_id INNER JOIN `admin` ON pengguna.admin_id = admin.admin_id WHERE admin.admin_id = '$adminID'";
                            $results = $db_handle->countQuery($query);
                            ?>
                            <div class="number count-to" data-from="0" data-to="<?php echo $results; ?>" data-speed="125" data-fresh-interval="20"></div>
                        </div>
                    </div>
                </div>
                
            </div>

            <div class="row clearfix">
                <!-- Task Info -->
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <div class="card">
                        <div class="header">
                            <h2>5 Acara Terbaru</h2>
                        </div>
                        <div class="body">
                            <div class="table-responsive">
                                <table class="table table-hover dashboard-task-infos">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>Nama Acara</th>
                                            <th>Lokasi Acara</th>
                                            <th>Tanggal Acara</th>
                                            <th>Waktu Acara</th>
                                            <th>Status</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    <?php
                                    //SELECT * FROM acara INNER JOIN pengguna ON acara.pengguna_id=pengguna.pengguna_id WHERE pengguna.admin_id = 'q9959534a236a0c3';
                                    $query=$mysqli->prepare('SELECT acara.nama_acara,acara.lokasi_acara,acara.tgl_acara,acara.waktu_acara,acara.status FROM acara INNER JOIN pengguna ON acara.pengguna_id=pengguna.pengguna_id WHERE pengguna.admin_id = ? ORDER BY acara.id DESC LIMIT 5');
                                    $query->bind_param('s', $adminID);
                                    $query->execute();
                                    $result=$query->get_result();
                                    $no = 0;
                                    while($row = $result->fetch_array()){
                                        $no++;
                                        if($row['status']==1){
                                            $statusAcara = "Open";
                                        }else{
                                            $statusAcara = "Closed";
                                        }
                                        echo '
                                        <tr>
                                            <td>'.$no.'</td>
                                            <td>'.$row['nama_acara'].'</td>
                                            <td>'.$row['lokasi_acara'].'</td>
                                            <td>'.$row['tgl_acara'].'</td>
                                            <td>'.$row['waktu_acara'].'</td>
                                            <td>'.$statusAcara.'</td>
                                        </tr>';
                                    }
                                    ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- #END# Task Info -->
            </div>
        </div>
    </section>
    
    <!-- Jquery Core Js -->
    <script src="/dash/plugins/jquery/jquery.min.js"></script>

    <!-- Bootstrap Core Js -->
    <script src="/dash/plugins/bootstrap/js/bootstrap.js"></script>

    <!-- Waves Effect Plugin Js -->
    <script src="/dash/plugins/node-waves/waves.js"></script>

    <!-- Jquery CountTo Plugin Js -->
    <script src="/dash/plugins/jquery-countto/jquery.countTo.js"></script>

    <!-- Autosize Plugin Js -->
    <script src="/dash/plugins/autosize/autosize.js"></script>

    <!-- Jquery DataTable Plugin Js -->
    <script src="/dash/plugins/jquery-datatable/jquery.dataTables.js"></script>
    <script src="/dash/plugins/jquery-datatable/skin/bootstrap/js/dataTables.bootstrap.js"></script>

    <!-- Custom Js -->
    <script src="/dash/js/admin.js"></script>
    <script src="/dash/js/index.js"></script>
    <script src="/dash/js/jquery-datatable.js"></script>
</body>

</html>
