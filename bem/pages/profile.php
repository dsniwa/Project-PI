<!-- Head -->
<?php include "head.php"; ?>
    
    <!-- Navigation BAR -->
    <?php include "navbar.php"; ?>

    <section class="content">
        <div class="container-fluid">
            <?php if(isset($_SESSION['notice'])){ ?>
                <div class="alert bg-green alert-dismissible" role="alert">
                    <?php echo $_SESSION['notice']; unset($_SESSION['notice']); ?>
                </div>
            <?php } ?>

            <?php
                $query=$mysqli->prepare('SELECT ganti_email FROM admin WHERE admin_id = ?');
                $query->bind_param('s', $adminID);
                $query->execute();
                $result=$query->get_result();
                while($row = $result->fetch_array()){
                    $ganti_email = $row['ganti_email'];
                }
                $query->close();
            ?>

            <?php if($ganti_email != ""){ ?>
                <div class="alert bg-orange alert-dismissible" role="alert">
                    Email <?php echo $ganti_email; ?> sedang menunggu konfirmasi. Jika link konfirmasi Anda hilang atau tidak menerima link konfirmasi pada email Anda, silahkan <a href="/bem/chresend.php" style="color:black;"> kirim ulang link konfirmasi</a>. Atau <a href="/bem/revoke.php" style="color:black;"> batalkan ganti email</a>.
                </div>
            <?php } ?>

            <?php if(isset($statusGanti)){
                if($statusGanti==1){
            ?>
                <div class="alert bg-green alert-dismissible" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                    Data Anda berhasil diubah.<?php if($adminChMail==true){ ?> Link konfirmasi telah dikirim ke email baru Anda <?php echo $newEmail; ?>, silahkan konfirmasi email tersebut untuk mengganti email Anda.<?php } ?>
                </div>
            <?php }else{ ?>
                <div class="alert bg-red alert-dismissible" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                    Data gagal diubah, silahkan coba beberapa saat lagi. Pesan error: Email telah digunakan pada akun lain atau pada akun Anda sendiri.
                </div>
            <?php } } ?>
            <!-- Exportable Table -->
            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <h2>UBAH DATA</h2>
                        </div>
                        <div class="body">
                            <div class="row clearfix">
                                <div class="col-sm-12">
                                <form id="form_validation" method="POST">
                                    <div class="form-group form-float">
                                        <div class="form-line">
                                            <input type="text" class="form-control" name="newAdminNama" value="<?php echo $adminNama; ?>" required />
                                            <label class="form-label">Nama BEM</label>
                                        </div>
                                    </div>

                                    <div class="form-group form-float">
                                        <div class="form-line">
                                            <input type="text" class="form-control" name="newAdminEmail" value="<?php echo $adminEmail; ?>" required />
                                            <label class="form-label">Email</label>
                                        </div>
                                    </div>
                                    
                                    <div class="form-group">
                                        <input type="submit" name="updateAdmin" class="btn btn-primary m-t-15 waves-effect" />
                                    </div>
                                </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- #END# Exportable Table -->
        </div>

        <!-- #Password -->
        <div class="container-fluid">
            <?php if(isset($chStatus)){
                if($chStatus == true){
            ?>
                <div class="alert bg-green alert-dismissible" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                    Password berhasil diubah.
                </div>
            <?php }else{ ?>
                <div class="alert bg-red alert-dismissible" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                    Password gagal diubah, silahkan coba lagi.
                </div>
            <?php } } ?>
            <!-- Exportable Table -->
            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <h2>UBAH PASSWORD</h2>
                        </div>
                        <div class="body">
                            <div class="row clearfix">
                                <div class="col-sm-12">
                                <form method="POST" onsubmit="return chkPwd()">
                                    <div class="form-group form-float">
                                        <div class="form-line">
                                            <input type="password" class="form-control" name="oldPw" required />
                                            <label class="form-label">Password Lama</label>
                                        </div>
                                    </div>

                                    <div class="form-group form-float">
                                        <div class="form-line">
                                            <input id="pass1" type="password" minlength="6" class="form-control" name="newPw" required />
                                            <label class="form-label">Password Baru (Minimal 6 huruf)</label>
                                        </div>
                                    </div>

                                    <div class="form-group form-float">
                                        <div class="form-line">
                                            <input id="pass2" type="password" minlength="6" class="form-control" name="confirm" required />
                                            <label class="form-label">Konfirmasi Password</label>
                                        </div>
                                    </div>
                                    
                                    <div class="form-group">
                                        <input type="submit" name="chpasswordAdmin" class="btn btn-primary m-t-15 waves-effect" />
                                    </div>
                                </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- #END# Exportable Table -->
        </div>
    </section>
    
    <!-- Jquery Core Js -->
    <script src="/dash/plugins/jquery/jquery.min.js"></script>

    <!-- Bootstrap Core Js -->
    <script src="/dash/plugins/bootstrap/js/bootstrap.js"></script>

    <!-- Slimscroll Plugin Js -->
    <script src="/dash/plugins/jquery-slimscroll/jquery.slimscroll.js"></script>

    <!-- Waves Effect Plugin Js -->
    <script src="/dash/plugins/node-waves/waves.js"></script>

    <!-- SweetAlert Plugin Js -->
    <script src="/dash/plugins/sweetalert/sweetalert.min.js"></script>

    <!-- Jquery Validation Plugin Css -->
    <script src="/dash/plugins/jquery-validation/jquery.validate.js"></script>
    <script src="/dash/plugins/jquery-validation/localization/messages_id.js"></script>

    <!-- Autosize Plugin Js -->
    <script src="/dash/plugins/autosize/autosize.js"></script>

    <!-- Moment Plugin Js -->
    <script src="/dash/plugins/momentjs/moment.js"></script>

    <!-- Custom Js -->
    <script src="/dash/js/admin.js"></script>
    <script src="/dash/js/form-validation.js"></script>
    <script src="/dash/js/form.js"></script>

    <script>
    function chkPwd() {
        var pass1 = document.getElementById("pass1").value;
        var pass2 = document.getElementById("pass2").value;
        var ok = true;
        if (pass1 != pass2) {
            swal("Error", "Passwords Do not match", "error");
            $( "#pass2" ).focus();
            ok = false;
        }
        return ok;
    }
    </script>
</body>

</html>
