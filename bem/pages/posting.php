<!-- Head -->
<?php include "head.php"; ?>
    
    <!-- Navigation BAR -->
    <?php include "navbar.php"; ?>

    <section class="content">
        <div class="container-fluid">
            <?php if(isset($status)){
                if($status=="1"){ ?>
                    <div class="alert bg-green alert-dismissible" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                        <?php echo $pesan; ?>
                    </div>
                    <?php 
                } 
                if($status=="2"){ ?>
                    <div class="alert bg-red alert-dismissible" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                        <?php echo $pesan; ?>
                    </div>
                    <?php
                }
            }
            if($_GET['x'] == "edit"){
            ?>
            <!-- Exportable Table -->
            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <?php
                            $postingID = $_GET['y'];
                            $query=$mysqli->prepare('SELECT * FROM posting WHERE admin_id = ? AND posting_id = ?');
                            $query->bind_param('ss', $adminID, $postingID);
                            $query->execute();
                            $result=$query->get_result();

                            while($row = $result->fetch_array()){
                                $judulPosting = $row['judul'];
                                $idPosting = $row['posting_id'];
                                $isiPosting = $row['isi'];
                            }
                            $query->close();
                        ?>
                        <div class="header">
                            <h2>EDIT POSTING - <?php echo $judulPosting; ?></h2>
                        </div>
                        <div class="body">
                            <div class="row clearfix">
                                <div class="col-sm-12">
                                <form id="form_validation" method="POST">
                                    <div class="form-group form-float">
                                        <input type="hidden" name="idPosting" value="<?php echo $idPosting; ?>">
                                        <textarea id="tinymce" name="isiPosting">
                                            <?php echo $isiPosting; ?>
                                        </textarea>
                                    </div>

                                    <div class="form-group">
                                        <input type="submit" name="editPosting" class="btn btn-block btn-primary m-t-15 waves-effect" />
                                    </div>
                                </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- #END# Exportable Table -->
            <?php
            }else{
            ?>
            <!-- Exportable Table -->
            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <h2>
                                DAFTAR POSTING ANDA
                            </h2>
                        </div>
                        <div class="body">
                            <table style="width: 100%; vertical-align: bottom;" class="table table-bordered table-striped table-hover dataTable table-basic">
                                <thead>
                                    <tr>
                                        <th class="middle">No</th>
                                        <th class="middle">Judul Posting / Judul Acara</th>
                                        <th class="middle">Waktu Posting</th>
                                        <th class="middle">Isi Posting</th>
                                        <th class="middle" style="text-align:center;">Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                <?php
                                    $query=$mysqli->prepare('SELECT * FROM posting WHERE admin_id = ?');
                                    $query->bind_param('s', $adminID);
                                    $query->execute();
                                    $result=$query->get_result();

                                    $no = 0;
                                    while($row = $result->fetch_array()){
                                        $no++;
                                        $isi = $row['isi'];
                                        if (strlen($row['isi']) > 100) {
                                            $isi = substr($row['isi'], 0, 100).".....";
                                        } else {
                                            $isi = $row['isi'];
                                        }
                                        echo '
                                            <tr>
                                                <td class="middle">'.$no.'</td>
                                                <td class="middle">'.$row['judul'].'</td>
                                                <td class="middle">'.$row['waktu'].'</td>
                                                <td class="middle">'.$isi.'</td>
                                                <td class="middle" style="text-align:center;" nowrap>

                                                    <a href="../edit/'.$row['posting_id'].'" title="Edit" class="btn btn-xs bg-blue waves-effect"><i class="material-icons">edit</i></a>

                                                    <a href="/events.php?events_id='.$row['posting_id'].'" target="_blank" title="View" class="btn btn-xs bg-green waves-effect"><i class="material-icons">remove_red_eye</i></a>

                                                    <a href="javascript:void(0)" data-value="'.$row['posting_id'].'" onclick="postadmDel(\''.$row['posting_id'].'\');" title="Delete" class="btn btn-danger btn-xs waves-effect"><i class="material-icons">delete</i></a><a href="javascript:void(0)" id="dt-del-btn-b"></a>

                                                </td>
                                            </tr>
                                        ';
                                    }
                                    ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <!-- #END# Exportable Table -->
            <?php } ?>
        </div>
    </section>
    
    <!-- Jquery Core Js -->
    <script src="/dash/plugins/jquery/jquery.min.js"></script>

    <!-- Bootstrap Core Js -->
    <script src="/dash/plugins/bootstrap/js/bootstrap.js"></script>

    <!-- Slimscroll Plugin Js -->
    <script src="/dash/plugins/jquery-slimscroll/jquery.slimscroll.js"></script>

    <!-- Waves Effect Plugin Js -->
    <script src="/dash/plugins/node-waves/waves.js"></script>

    <!-- Jquery Validation Plugin Js -->
    <script src="/dash/plugins/jquery-validation/jquery.validate.js"></script>
    <script src="/dash/plugins/jquery-validation/localization/messages_id.js"></script>

    <!-- Jquery DataTable Plugin Js -->
    <script src="/dash/plugins/jquery-datatable/jquery.dataTables.js"></script>
    <script src="/dash/plugins/jquery-datatable/skin/bootstrap/js/dataTables.bootstrap.js"></script>
    <script src="/dash/plugins/jquery-datatable/extensions/export/dataTables.buttons.min.js"></script>
    <script src="/dash/plugins/jquery-datatable/extensions/export/buttons.flash.min.js"></script>
    <script src="/dash/plugins/jquery-datatable/extensions/export/jszip.min.js"></script>
    <script src="/dash/plugins/jquery-datatable/extensions/export/pdfmake.min.js"></script>
    <script src="/dash/plugins/jquery-datatable/extensions/export/vfs_fonts.js"></script>
    <script src="/dash/plugins/jquery-datatable/extensions/export/buttons.html5.min.js"></script>
    <script src="/dash/plugins/jquery-datatable/extensions/export/buttons.print.min.js"></script>

    <script src="https://cdn.datatables.net/fixedcolumns/3.2.2/js/dataTables.fixedColumns.min.js"></script>

    <!-- Select Plugin Js -->
    <script src="/dash/plugins/bootstrap-select/js/bootstrap-select.js"></script>

    <!-- Autosize Plugin Js -->
    <script src="/dash/plugins/autosize/autosize.js"></script>

    <!-- Moment Plugin Js -->
    <script src="/dash/plugins/momentjs/moment.js"></script>

    <!-- Bootstrap Material Datetime Picker Plugin Js -->
    <script src="/dash/plugins/bootstrap-material-datetimepicker/js/bootstrap-material-datetimepicker.js"></script>
    
    <!-- TinyMCE -->
    <script src="/dash/plugins/tinymce/tinymce.js"></script>

    <!-- SweetAlert Plugin Js -->
    <script src="/dash/plugins/sweetalert/sweetalert.min.js"></script>

    <!-- Custom Js -->
    <script src="/dash/js/admin.js"></script>
    <script src="/dash/js/jquery-datatable.js"></script>
    <script src="/dash/js/ajax-func.js"></script>
    <script src="/dash/js/basic-form-elements.js"></script>
    <script src="/dash/js/form-validation.js"></script>
    <script src="/dash/js/tooltips-popovers.js"></script>
    <script src="/dash/js/editors.js"></script>
</body>

</html>
