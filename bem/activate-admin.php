
<!-- Head -->
<?php include "pages/head.php"; ?>
    
    <!-- Navigation BAR -->
    <?php include "pages/navbar.php"; ?>

    <section class="content">
        <div class="container-fluid">
            <div class="block-header">
                <div class="row clearfix">
                    <div class="col-sm-6"><h2>Please Activate Your Account</h2></div>
                    <div class="col-sm-6"><span style="float: right;">Terakhir Login: <?php echo $_SESSION['last_loginAdmin']; ?></span></div>
                </div>
            </div>

        <div class="row clearfix">
            <?php if(isset($_GET['regenerate'])){ ?>
                <div class="alert bg-green alert-dismissible" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                    Email telah dikirim, silahkan cek email Anda.
                </div>
            <?php } ?>
                <!-- Task Info -->
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <div class="card">
                        <div class="header">
                            <h4>Email terhapus? <a href="/bem/regenerate.php">Klik di sini</a> untuk mendapatkan link konfirmasi yang baru</h4>
                        </div>
                    </div>
                </div>
                <!-- #END# Task Info -->
            </div>
    </section>
    
    <!-- Jquery Core Js -->
    <script src="/dash/plugins/jquery/jquery.min.js"></script>

    <!-- Bootstrap Core Js -->
    <script src="/dash/plugins/bootstrap/js/bootstrap.js"></script>

    <!-- Slimscroll Plugin Js -->
    <script src="/dash/plugins/jquery-slimscroll/jquery.slimscroll.js"></script>

    <!-- Waves Effect Plugin Js -->
    <script src="/dash/plugins/node-waves/waves.js"></script>

    <!-- Jquery CountTo Plugin Js -->
    <script src="/dash/plugins/jquery-countto/jquery.countTo.js"></script>

    <!-- Autosize Plugin Js -->
    <script src="/dash/plugins/autosize/autosize.js"></script>

    <!-- SweetAlert Plugin Js -->
    <script src="/dash/plugins/sweetalert/sweetalert.min.js"></script>

    <!-- Custom Js -->
    <script src="/dash/js/admin.js"></script>
    <script src="/dash/js/index.js"></script>
    <script src="/dash/js/ajax-func.js"></script>
</body>

</html>
