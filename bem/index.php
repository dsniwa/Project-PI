﻿<?php
// sertakan berkas utama
$role = "admin";
require_once $_SERVER['DOCUMENT_ROOT'].'/include/load.php';

if(empty($_SESSION['UIDAdmin'])){
    if($enableCaptchaAdmin==false){
        include 'pages/login.php';
    }else if($enableCaptchaAdmin==true){
        include 'pages/login-captcha.php';
    }
die();
}

// Set page
if(!isset($_GET['page'])){
    $page = "";
}else{
    $page = $_GET['page'];
}

if($adminStatus == "0"){
    $pageTitle = "Aktifasi | Panel BEM";
    include 'activate-admin.php';
    exit;
}

if($page == "dashboard"){
	$pageTitle = "Dashboard | Panel BEM";
    include "pages/dashboard.php";
}else if($page == "ketuplak"){
	$pageTitle = "Daftar Ketuplak | Panel BEM";
	// Tambah user
	if(isset($_POST['newRegister'])){
		include 'action/add-user.php';
	}
    include "pages/user.php";
}else if($page == "profile"){
	$pageTitle = "Profile | Panel BEM";

	// Edit biodata
	if(isset($_POST['updateAdmin'])){
		include 'action/edit-profile.php';
	}

	// Ganti Password
	if(isset($_POST['chpasswordAdmin'])){
		include 'action/edit-password.php';
	}

    include "pages/profile.php";

// Halaman posting
}else if($page == "posting"){
    if(!isset($_GET['x'])){header('Location: /bem/posting/view/');}
    
    // View acara
    if($_GET['x']=="view"){
        $pageTitle = "Daftar Posting | Panel BEM";
        include 'pages/posting.php';
        die;
    }

    if($_GET['x']=="edit"){
        $pageTitle = "Edit Posting | Panel BEM";
        if(isset($_POST['editPosting'])){
            include 'action/edit-posting.php';
        }
        include 'pages/posting.php';
        die;
    }

}else if($page == ""){
    header("Location: /bem/dashboard/");
}else{
    header("Location: /bem/dashboard/");
}
?>
