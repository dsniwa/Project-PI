<?php
// sertakan berkas utama
$role = "admin";
require_once $_SERVER['DOCUMENT_ROOT'].'/include/load.php';

// Cek status login
if(empty($_SESSION['UIDAdmin'])){
header("HTTP/1.1 404 Not Found");
die();
}
    // String
    $idPengguna = $_POST['id'];
    $status = 0;

    // Delete peserta
    $query=$mysqli->prepare("DELETE FROM peserta WHERE pengguna_id = ?");
    $query->bind_param('s', $idPengguna);
    if($query->execute()){
        $status = $status + 1;
    }
    $query->close();

    // Delete acara
    $query=$mysqli->prepare("DELETE FROM acara WHERE pengguna_id = ?");
    $query->bind_param('s', $idPengguna);
    if($query->execute()){
        $status = $status + 1;
    }
    $query->close();

    // Delete pengguna
    $query=$mysqli->prepare("DELETE FROM pengguna WHERE pengguna_id = ?");
    $query->bind_param('s', $idPengguna);
    if($query->execute()){
        $status = $status + 1;
    }
    $query->close();

    if ($status == 3) { 
        $response_array['status'] = 'success';
    } else {
        $response_array['status'] = 'failed';
    }
    header('Content-type: application/json');
    echo json_encode($response_array);
?>
