<?php
// sertakan berkas utama
$role = "user";
require_once $_SERVER['DOCUMENT_ROOT'].'/include/load.php';

// if(empty($_SESSION['UIDSuperAdmin'])){
//     if($enableCaptchaAdmin==false){
//         include 'pages/login.php';
//     }else if($enableCaptchaAdmin==true){
//         include 'pages/login-captcha.php';
//     }
// die();
// }

if(!isset($_GET['page'])){
    $page = "";
}else{
    $page = $_GET['page'];
}

if($page == "registration"){
    if(isset($_POST['registNewUser'])){
        // Waktu daftar
        $now = date('Y-m-d');
        $today = new DateTime($now);
        $regtimeTmp = $today->format('Y-m-d');
        // Data
        $peserta_id = randomKey();
        $npm = $_POST['newNPM'];
        $nama = $_POST['newNama'];
        $email = $_POST['newEmail'];
        $acara = $_POST['newAcara'];
        $user = $userID;
        $regtime = $regtimeTmp;
        // Hitung
        $query = $mysqli->prepare('SELECT COUNT(id) FROM peserta WHERE npm = ? AND acara_id = ?');
        $query->bind_param('ss', $npm, $acara);
        $query->execute();
        $result=$query->get_result();
        $row = $result->fetch_row();
        $jumlah = $row[0];

        // Get nama acara
        $queryAcara=$mysqli->prepare('SELECT nama_acara FROM acara WHERE acara_id = ?');
        $queryAcara->bind_param('s', $acara);
        $queryAcara->execute();
        $result = $queryAcara->get_result();
        while($row = $result->fetch_array()){
            $namaAcara = $row['nama_acara'];
        }
        $queryAcara->close();

        if($jumlah==1){
            $statusDaftarBerhasil = false;
        }else if($jumlah==0){
            $query = $mysqli->prepare('INSERT INTO peserta (peserta_id, npm, nama, email, waktu_daftar, acara_id, pengguna_id)values(?, ?, ?, ?, ?, ?, ?)');
            $query->bind_param('sssssss', $peserta_id, $npm, $nama, $email, $regtime, $acara, $user);
            $query->execute();
            $statusDaftarBerhasil = true;
            require_once "notify.mail.php";
        }else{
            $statusDaftarBerhasil = false;
        }
    }
    include "pages/registrasi.php";
}else if($page == "profile"){
    if(isset($_POST['lengkapiProfile'])){
        // Waktu daftar
        $date = date("Y-m-d");
        $tanggal = formatIndonesia($date, true);
        $waktu = date("H:i:s");
        $actStamp = $tanggal." | ".$waktu;

        $peserta_id = $_POST['peserta_id'];
        $domisili = $_POST['domisili'];
        $var_satu = $_POST['varOne'];
        $var_dua = $_POST['varTwo'];
        $no_hp = $_POST['hp'];
        $tgl_lhr = $_POST['tglLhr'];
        $waktu_aktifasi = $actStamp;
        $status = '1';
 
        $query = $mysqli->prepare('UPDATE peserta SET domisili = ?, var_satu = ?, var_dua = ?, no_hp = ?, tgl_lahir = ?, waktu_aktifasi = ?, status = ? WHERE peserta_id = ?');
        $query->bind_param('ssssssss', $domisili, $var_satu, $var_dua, $no_hp, $tgl_lhr, $waktu_aktifasi, $status, $peserta_id);
        if ($query->execute()) { 
            require_once "ticket.factory.php";
            $response_array['status'] = 'success';
        } else {
            $response_array['status'] = 'failed';
        }
        header('Content-type: application/json');
        echo json_encode($response_array);
        exit;
    }

    
    // Pilih acara
    if(isset($_GET['x'],$_GET['y'],$_GET['z'])){
        $db_handle = new DBController();
        $idAcara = $_GET['z'];
        $query ="SELECT COUNT(id) FROM `acara` WHERE `acara_id` = '".$idAcara."'";
        $results = $db_handle->countQuery($query);
        if($results < 1){
            $pilihanSalahErr = true;
        }else{
            include "pages/profile-isi.php";
            exit;
        }
    }

    // Isi profile
    if(isset($_POST['nextProfile'])){
        $bem = $_POST['bem'];
        $acara = $_POST['acara'];
        $pilihan = $_POST['pilihan'];
        header("Location: /portal/profile/".$bem."/".$acara."/".$pilihan."/");
    }

    // Default
    include "pages/profile.php";
}else if($page == "ticket"){
    ///include "welcome.php";
    header("Location: /portal/profile/");
}else{
    //include "welcome.php";
    header("Location: /portal/profile/");
}
?>
