<?php
// sertakan berkas utama
$role = "none";
require_once $_SERVER['DOCUMENT_ROOT'].'/include/load.php';

$db_handle = new DBController();
if(!empty($_POST["q"])) {
	$q=$_POST['q'];
	$query ="SELECT acara.acara_id,acara.nama_acara,acara.tgl_acara,acara.lokasi_acara,admin.admin_id,pengguna.pengguna_id,pengguna.nama AS nama_pengguna,admin.nama AS nama_admin FROM acara INNER JOIN pengguna ON acara.pengguna_id=pengguna.pengguna_id INNER JOIN admin ON admin.admin_id=pengguna.admin_id WHERE nama_acara LIKE '%$q%'";
	$results = $db_handle->selectQuery($query);
?>
<?php
	if($results==0){
		echo "Duh, acara tidak ditemukan :(<br/>Coba periksa kembali apakah ada typo pada keyword Anda.";
	}else{
		echo "Berikut acara yang ditemukan berdasarkan keyword Anda:";
		foreach($results as $acara) {
		echo '<a href="/portal/profile/'.$acara['admin_id'].'/'.$acara['pengguna_id'].'/'.$acara['acara_id'].'/" class="list-group-item"><h4 class="list-group-item-heading">'.$acara['nama_acara'].'</h4><p class="list-group-item-text">Acara ini diselenggarakan oleh '.$acara['nama_admin'].' dan merupakan rangkaian dari acara '.$acara['nama_pengguna'].'<br/>Lokasi acara : '.$acara['lokasi_acara'].'<br/>Tanggal acara : '.$acara['tgl_acara'].'</p></a>';
		}
	}
}
?>
