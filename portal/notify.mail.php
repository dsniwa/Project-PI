<?php
require_once $_SERVER['DOCUMENT_ROOT'].'/lib/PHPMailer/PHPMailerAutoload.php';

// Cek status login
if(empty($_SESSION['UIDUser'])){
    header("HTTP/1.1 404 Not Found");
    die();
}

function get_include_contents($filename, $variablesToMakeLocal) {
    extract($variablesToMakeLocal);
    if (is_file($filename)) {
        ob_start();
        include $filename;
    return ob_get_clean();
    }
    return false;
}

$data['judul'] = "Pemberitahuan pendaftaran acara $namaAcara";
$data['header'] = "Harap segera lengkapi data Anda untuk mendapatkan tiket.";
$data['one'] = "Halo $nama,";
$data['two'] = "Email ini merupakan pemberitahuan Anda telah terdaftar pada acara $namaAcara, harap segera lengkapi data diri Anda pada halaman ini dan ikuti petunjuk yang ada.";
$data['link'] = $_SERVER['SERVER_NAME']."/portal/";
$data['button'] = "Lengkapi Data Diri";
$data['three'] = "Jika tombol di atas tidak dapat di klik, silahkan copy & paste link di bawah ini ke address bar browser Anda.<br/>".$_SERVER['SERVER_NAME']."/portal/";
$data['four'] = "Setelah melengkapi data diri Anda akan menerima email yang berisika E-Tiket Anda.";

$mail = new PHPMailer;

//$mail->SMTPDebug = 4;

$mail->isSMTP();// Set mailer to use SMTP
$mail->Host         = $config->mail->host;// Specify main and backup SMTP servers
$mail->SMTPAuth     = true;// Enable SMTP authentication
$mail->Username     = $config->mail->norepUser;// SMTP username
$mail->Password     = $config->mail->norepPass;// SMTP password
$mail->SMTPSecure   = 'tls';// Enable TLS encryption, `ssl` also accepted
$mail->Port         = 587;// TCP port to connect to

$mail->setFrom($config->mail->norepMail, 'KopiAdem noreply');
$mail->addAddress($email, $nama);// Add a recipient
//$mail->AddBCC('no-reply@events.kopiadem.com');// Send a copy to our server

$mail->isHTML(true);// Set email format to HTML

$mail->Subject = "Pemberitahuan pendaftaran acara $namaAcara";
$mail->Body    = get_include_contents($_SERVER['DOCUMENT_ROOT'].'/lib/email-template.php', $data); 
if(!$mail->send()) {
    echo 'Message could not be sent.<br/>';
    echo 'Mailer Error: ' . $mail->ErrorInfo;
}
?>
