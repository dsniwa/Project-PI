<?php
// Denied direct access
if(!isset($secretKey)){
    header("HTTP/1.1 404 Not Found");
    die();
}else{
    if($secretKey != "cbdd76aa9ebb390b4e5c68f8ee51f3b6"){
        header("HTTP/1.1 404 Not Found");
        die();
    }
}

require_once $_SERVER['DOCUMENT_ROOT'].'/lib/PHPMailer/PHPMailerAutoload.php';

function get_include_contents($filename, $variablesToMakeLocal) {
    extract($variablesToMakeLocal);
    if (is_file($filename)) {
        ob_start();
        include $filename;
    return ob_get_clean();
    }
    return false;
}

$data['judul'] = "Tiket acara $acara";
$data['header'] = "Halo $nama, tiket Anda berada pada lampiran email ini.";
$data['one'] = "Halo $nama,";
$data['two'] = "Terimakasih telah mendaftar pada acara $acara dan melengkapi data diri, tiket Anda ada pada lampiran email ini.";
$data['link'] = "";
$data['button'] = "";
$data['three'] = "Silahkan download tiket Anda dan print untuk dibawa pada saat acara berlangsung, atau sesuai kebijakan panitia tiket dapat dibawa dalam bentuk elektronik seperti file pdf yang ditunjukkan pada saat acara berlangsung.";
$data['four'] = "Selamat menikmati acara, semoga Anda senang.";

$mail = new PHPMailer;

//$mail->SMTPDebug = 4;

$mail->isSMTP();// Set mailer to use SMTP
$mail->Host         = $config->mail->host;// Specify main and backup SMTP servers
$mail->SMTPAuth     = true;// Enable SMTP authentication
$mail->Username     = $config->mail->norepUser;// SMTP username
$mail->Password     = $config->mail->norepPass;// SMTP password
$mail->SMTPSecure   = 'tls';// Enable TLS encryption, `ssl` also accepted
$mail->Port         = 587;// TCP port to connect to

$mail->setFrom($config->mail->norepMail, 'KopiAdem noreply');
$mail->addAddress($email, $nama);// Add a recipient
//$mail->AddBCC('no-reply@events.kopiadem.com');// Send a copy to our server

$mail->isHTML(true);// Set email format to HTML

$mail->addAttachment($_SERVER["DOCUMENT_ROOT"].'/include/ticketTmp/'.$noReg.'-E-TICKET '.$acara.'.pdf', 'E-Ticket '.$acara.'.pdf');// Optional name

$mail->Subject = "Tiket acara $acara";
$mail->Body    = get_include_contents($_SERVER['DOCUMENT_ROOT'].'/lib/email-template.php', $data); 
if(!$mail->send()) {
    echo 'Message could not be sent.<br/>';
    echo 'Mailer Error: ' . $mail->ErrorInfo;
}
?>
