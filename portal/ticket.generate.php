<?php
// Denied direct access
if(!isset($secretKey)){
    header("HTTP/1.1 404 Not Found");
    die();
}else{
    if($secretKey != "cbdd76aa9ebb390b4e5c68f8ee51f3b6"){
        header("HTTP/1.1 404 Not Found");
        die();
    }
}

// Siapkan data tiket
$query=$mysqli->prepare('SELECT peserta.id,peserta.peserta_id,peserta.nama,peserta.npm,peserta.no_hp,peserta.tgl_lahir,peserta.email,acara.nama_acara,acara.lokasi_acara,acara.tgl_acara,acara.waktu_acara FROM peserta INNER JOIN acara ON peserta.acara_id=acara.acara_id WHERE peserta.peserta_id = ?');
$query->bind_param('s', $peserta_id);
$query->execute();
$result = $query->get_result();
while($row = $result->fetch_array()){
    $noReg = $row['id'];
    $kodeReg = $row['peserta_id'];
    $acara = $row['nama_acara'];
    $lokasi = $row['lokasi_acara'];
    $tanggal = $row['tgl_acara'];
    $waktu = $row['waktu_acara'];
    $nama = $row['nama'];
    $npm = $row['npm'];
    $telp = $row['no_hp'];
    $dob = $row['tgl_lahir'];
    $email = $row['email'];
}
$query->close();

// Start dompdf
require_once $_SERVER['DOCUMENT_ROOT'].'/lib/dompdf/autoload.inc.php';

// reference the Dompdf namespace
use Dompdf\Dompdf;

// Define host/domain
$host = $_SERVER["SERVER_NAME"];
$apiURL = "$host/ticket/verify/$kodeReg";

// instantiate and use the dompdf class
$dompdf = new Dompdf();
$dompdf->set_option('defaultFont', 'Helvetica');
$dompdf->loadHtml('
<div style="margin-top: 5px; margin-left: auto; margin-right: auto; width: 95%; padding: 30px;">
<table border="1" style="border-collapse:collapse; text-align:center; width:100%;">
<tr>
<td colspan="3" style="padding:10px 0 10px 0;"><h2 style="display:inline;">Tiket '.$acara.'</h2></td>
</tr>
<tr>
<th>Event Info</th><th>Ticket Info</th><th>Kode Tiket</th>
</tr>
<tr>
<td width="auto" style="padding:5px;">
Lokasi :<br/> <strong>'.$lokasi.'</strong><br/><br/>
Tanggal :<br/> <strong>'.$tanggal.'</strong> <br/><br/>
Waktu :<br/> <strong>'.$waktu.'</strong> <br/>
</td>
<td width="auto" style="padding:5px; text-align:left;">
Nama : <strong>'.$nama.'</strong><br/><br/>
ID : <strong>'.$npm.'</strong><br/><br/>
Phone : <strong>'.$telp.'</strong><br/><br/>
DOB : <strong>'.$dob.'</strong><br/>
</td>
<td style="text-align:center; padding:0 5px 5px 5px;" width="30.33%">
<img src="http://'.$host.'/lib/phpqrcode/qr.php?qr='.$apiURL.'" width="180px"/><br/>
<img src="http://'.$host.'/lib/Barcode/bar.php?number='.$noReg.'" width="160px"/><br/>
'.$noReg.'
</td>
</tr>
</table>
<br/>
<br/>
</div>
');

// (Optional) Setup the paper size and orientation
$dompdf->setPaper('A5', 'landscape');

// Render the HTML as PDF
$dompdf->render();

// Save file
$output = $dompdf->output();
$file_to_save = $_SERVER["DOCUMENT_ROOT"].'/include/ticketTmp/'.$noReg.'-E-TICKET '.$acara.'.pdf';
file_put_contents($file_to_save, $output);

// Output the generated PDF to Browser
//$dompdf->stream("Ticket");

?>
