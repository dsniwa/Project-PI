<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <title>Portal | Acara Online</title>
    <!-- Favicon-->
    <link rel="icon" href="../../favicon.ico" type="image/x-icon">

    <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,700&subset=latin,cyrillic-ext" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet" type="text/css">

    <!-- Bootstrap Core Css -->
    <link href="/dash/plugins/bootstrap/css/bootstrap.css" rel="stylesheet">

    <!-- Waves Effect Css -->
    <link href="/dash/plugins/node-waves/waves.css" rel="stylesheet" />

    <!-- Animation Css -->
    <link href="/dash/plugins/animate-css/animate.css" rel="stylesheet" />

    <!-- Bootstrap Select Css -->
    <link href="/dash/plugins/bootstrap-select/css/bootstrap-select.css" rel="stylesheet" />

    <!-- Custom Css -->
    <link href="/dash/css/style.css" rel="stylesheet">
</head>

<body class="signup-page custom-bg" style="max-width: 600px;">
    <div class="signup-box">
        <div class="logo">
            <a href="/portal/">Portal</a>
            <small>Selamat datang di portal Acara Online Gunadarma</small>
        </div>
        <div class="card">
            <div class="header">
                <center>Pilih atau cari acara yang Anda ikuti.</center>
            </div>
            <div class="body">
                <!-- Nav tabs -->
                <ul class="nav nav-tabs tab-nav-right" role="tablist">
                    <li role="presentation" class="active"><a href="#selectTab" data-toggle="tab"><i class="material-icons">touch_app</i> PILIH</a></li>
                    <li role="presentation"><a href="#searchTab" data-toggle="tab"><i class="material-icons">search</i> CARI</a></li>
                </ul>

                <!-- Tab panes -->
                <div class="tab-content">
                    <div role="tabpanel" class="tab-pane fade in active" id="selectTab">
                        <div class="div-modal"></div>
                        <form id="sign_up" method="POST">
                            <?php if(isset($pilihanSalahErr)){ ?>
                            <div class="alert bg-orange alert-dismissible" role="alert">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                                Alamat URL tidak valid.
                            </div>
                            <?php } ?>
                            <div class="form-group">
                                <h2 class="card-inside-title">Organisasi</h2>
                                <select class="form-control" name="bem" id="bem" onChange="getAcara(this.value);" required="">
                                    <option value="" disabled="" selected="">-- Pilih Organisasi --</option>
                                    <?php
                                    $db_handle = new DBController();
                                    $query ="SELECT * FROM admin";
                                    $results = $db_handle->selectQuery($query);
                                    ?>
                                    <?php
                                    foreach($results as $bem) {
                                    ?>
                                    <option value="<?php echo $bem["admin_id"]; ?>"><?php echo $bem["nama"]; ?></option>
                                    <?php } ?>
                                </select>
                            </div>

                            <div class="form-group">
                                <h2 class="card-inside-title">Penyelenggara</h2>
                                <select class="form-control" onChange="getPilihan(this.value);" name="acara" id="acara" required="">
                                    <option value="" disabled="" selected="">-- Pilih Penyelenggara --</option>
                                </select>
                            </div>

                            <div class="form-group">
                                <h2 class="card-inside-title">Acara</h2>
                                <select class="form-control" name="pilihan" id="pilihan" required="">
                                    <option value="" disabled="" selected="">-- Pilih Acara --</option>
                                </select>
                            </div>

                            <div class="input-group">
                                <input class="btn btn-block btn-lg bg-blue waves-effect" type="submit" name="nextProfile" value="Selanjutnya">
                            </div>
                        </form>
                    </div>

                    <div role="tabpanel" class="tab-pane fade" id="searchTab">
                        <form id="searchAcara" method="post">
                            <div class="form-group form-float" style="margin-bottom: 0px; margin-top: 5px;">
                                <div class="row">
                                    <div class="col-sm-11">
                                        <div class="form-line">
                                            <input id="newAcara" type="text" class="form-control" name="newAcara" required />
                                            <label class="form-label">Acara...</label>
                                        </div>
                                    </div>
                                    <div class="col-sm-1">
                                        <div class="form-line">
                                            <button type="submit" class="btn btn-primary waves-effect pull-right" onclick="getSearchAcara();"><i class="material-icons">search</i></button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>

                        <div class="list-group" id="hasil-cari-acara"></div>

                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Jquery Core Js -->
    <script src="/dash/plugins/jquery/jquery.min.js"></script>

    <!-- Bootstrap Core Js -->
    <script src="/dash/plugins/bootstrap/js/bootstrap.js"></script>

    <!-- Waves Effect Plugin Js -->
    <script src="/dash/plugins/node-waves/waves.js"></script>

    <!-- Select Plugin Js -->
    <script src="/dash/plugins/bootstrap-select/js/bootstrap-select.js"></script>

    <!-- Validation Plugin Js -->
    <script src="/dash/plugins/jquery-validation/jquery.validate.js"></script>

    <!-- Custom Js -->
    <script src="/dash/js/admin.js"></script>
    <script src="/dash/js/ajax-func.js"></script>
</body>

</html>
