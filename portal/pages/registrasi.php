<?php
if(empty($_SESSION['UIDUser'])){
    if($enableCaptcha==false){
        include 'login.php';
    }else if($enableCaptcha==true){
        include 'login-captcha.php';
    }
die();
}
?>
<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <title>Portal | Acara Online</title>
    <!-- Favicon-->
    <link rel="icon" href="../../favicon.ico" type="image/x-icon">

    <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,700&subset=latin,cyrillic-ext" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet" type="text/css">

    <!-- Bootstrap Core Css -->
    <link href="/dash/plugins/bootstrap/css/bootstrap.css" rel="stylesheet">

    <!-- Waves Effect Css -->
    <link href="/dash/plugins/node-waves/waves.css" rel="stylesheet" />

    <!-- Animation Css -->
    <link href="/dash/plugins/animate-css/animate.css" rel="stylesheet" />

    <!-- Bootstrap Select Css -->
    <link href="/dash/plugins/bootstrap-select/css/bootstrap-select.css" rel="stylesheet" />

    <!-- Custom Css -->
    <link href="/dash/css/style.css" rel="stylesheet">
</head>

<body class="signup-page custom-bg" style="max-width: 600px;">
    <div class="signup-box">
        <div class="logo">
            <a href="/portal/">Portal</a>
            <small>Selamat datang di portal Acara Online Gunadarma</small>
        </div>
        <div class="card">
            <div class="body">
                <form id="registNewUser" method="POST">
                    <div class="msg">Pilih acara yang diikuti</div>
                    <?php if(isset($statusDaftarBerhasil)){ ?>
                        <?php if($statusDaftarBerhasil==true){ ?>
                            <div id="autoHideElement" class="alert bg-green" role="alert">
                                Peserta berhasil didaftarkan.
                            </div>
                        <?php } ?>
                        <?php if($statusDaftarBerhasil==false){ ?>
                            <div class="alert bg-red alert-dismissible" role="alert">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                                Peserta gagal didaftarkan karena peserta dengan NPM tersebut telah terdaftar atau terjadi kesalahan sistem, silahkan coba lagi.
                            </div>
                        <?php } ?>
                    <?php } ?>
                    <div class="form-group form-float">
                        <div class="form-line">
                            <input type="text" class="form-control" name="newNPM" required />
                            <label class="form-label">NPM / No. Identitas</label>
                        </div>
                    </div>

                    <div class="form-group form-float">
                        <div class="form-line">
                            <input type="text" class="form-control" name="newNama" required />
                            <label class="form-label">Nama Lengkap</label>
                        </div>
                    </div>

                    <div class="form-group form-float">
                        <div class="form-line">
                            <input type="text" class="form-control" name="newEmail" required />
                            <label class="form-label">Email</label>
                        </div>
                    </div>

                    <div class="form-group">
                        <h2 class="card-inside-title">Acara</h2>
                        <select class="form-control" name="newAcara" id="acara" required="">
                            <option value="" disabled selected="">-- Pilih Acara --</option>
                            <?php
                            $db_handle = new DBController();
                            $query ="SELECT acara_id,nama_acara,status FROM acara WHERE pengguna_id='$userID'";
                            $results = $db_handle->selectQuery($query);
                            ?>
                            <?php
                            foreach($results as $acara) {
                                if($acara['status']==2){
                                    $status = 2;
                                }else{
                                    $status = statusAcara($acara['acara_id']);
                                }
                            ?>
                            <option value="<?php echo $acara["acara_id"]; ?>" <?php if($status==2){echo "disabled";} ?>><?php if($status==2){echo "[CLOSED] ";} echo $acara["nama_acara"];?></option>
                            <?php } ?>
                        </select>
                    </div>

                    <div class="input-group">
                        <input class="btn btn-block btn-lg bg-blue waves-effect" type="submit" name="registNewUser" value="Daftar">
                    </div>

                </form>
            </div>
        </div>
    </div>
    <div class="modal"></div>

    <!-- Jquery Core Js -->
    <script src="/dash/plugins/jquery/jquery.min.js"></script>

    <!-- Bootstrap Core Js -->
    <script src="/dash/plugins/bootstrap/js/bootstrap.js"></script>

    <!-- Waves Effect Plugin Js -->
    <script src="/dash/plugins/node-waves/waves.js"></script>

    <!-- Select Plugin Js -->
    <script src="/dash/plugins/bootstrap-select/js/bootstrap-select.js"></script>

    <!-- Validation Plugin Js -->
    <script src="/dash/plugins/jquery-validation/jquery.validate.js"></script>

    <!-- Custom Js -->
    <script src="/dash/js/admin.js"></script>
    <script src="/dash/js/ajax-func.js"></script>
    <script src="/dash/js/auto-hide.js"></script>
</body>

</html>
