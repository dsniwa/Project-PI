/*
 Navicat Premium Data Transfer

 Source Server         : Localhost
 Source Server Type    : MySQL
 Source Server Version : 50723
 Source Host           : localhost:3306
 Source Schema         : acara

 Target Server Type    : MySQL
 Target Server Version : 50723
 File Encoding         : 65001

 Date: 22/08/2018 20:36:47
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for acara
-- ----------------------------
DROP TABLE IF EXISTS `acara`;
CREATE TABLE `acara`  (
  `id` int(5) NOT NULL AUTO_INCREMENT,
  `acara_id` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `nama_acara` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `lokasi_acara` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `tgl_acara` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `waktu_acara` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `kuota` int(10) NOT NULL,
  `status` int(1) NOT NULL DEFAULT 1,
  `jenis` int(1) NOT NULL,
  `pengguna_id` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 9 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of acara
-- ----------------------------
INSERT INTO `acara` VALUES (5, 'a5h5974bb2f53e5c', 'Seminar Linux', 'Depok', '23-07-2017', '08:00 - 10:00', 150, 2, 1, '9g55974b1e732ebc');
INSERT INTO `acara` VALUES (6, 'rvo597da9e08e82c', 'Seminar Windows', 'Depok', '07-08-2017', '08:00 - 10:00', 150, 1, 0, '9g55974b1e732ebc');
INSERT INTO `acara` VALUES (7, 'nni597daa1e1d02f', 'Workshop Linux', 'Kalimalang', '07-08-2017', '08:30 - 11:30', 75, 1, 0, '9g55974b1e732ebc');
INSERT INTO `acara` VALUES (8, '1sf597dad9583758', 'Basic Overcloacking', 'Depok', '07-08-2017', '08:00 - 10:00', 100, 1, 0, 'ygw597d3ce45c66e');

-- ----------------------------
-- Table structure for admin
-- ----------------------------
DROP TABLE IF EXISTS `admin`;
CREATE TABLE `admin`  (
  `id` int(5) NOT NULL AUTO_INCREMENT,
  `admin_id` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `username` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `hash` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `nama` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `email` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `waktu_daftar` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `login_terakhir` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL DEFAULT 'never',
  `email_status` int(1) NOT NULL DEFAULT 0,
  `email_hash` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `ganti_email` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `username`(`username`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 8 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of admin
-- ----------------------------
INSERT INTO `admin` VALUES (1, '4r35968ac9091261', 'fikti', '$2y$10$9pS63XqmiEjQSBGskVh7POBh/LbVnBGbCQK6gpv/sRTsShMmIrQf2', 'BEM FIKTI', 'fikti@ug.kopiadem.com', '2017-07-14', 'Selasa, 08 Mei 2018 | 10:32:30', 1, 'Active', '');
INSERT INTO `admin` VALUES (2, 'itj5968acbfafe2d', 'fti', '$2y$10$9pS63XqmiEjQSBGskVh7POBh/LbVnBGbCQK6gpv/sRTsShMmIrQf2', 'BEM FTI', 'fti@ug.kopiadem.com', '2017-07-14', 'Minggu, 30 Juli 2017 | 09:00:25', 1, 'Active', '');
INSERT INTO `admin` VALUES (3, 'psd5968af8a0cf08', 'ekonomi', '$2y$10$9pS63XqmiEjQSBGskVh7POBh/LbVnBGbCQK6gpv/sRTsShMmIrQf2', 'BEM FE', 'ekonomi@ug.kopiadem.com', '2017-07-14', 'Minggu, 30 Juli 2017 | 09:04:42', 1, 'Active', '');
INSERT INTO `admin` VALUES (4, 'veg5968afee78956', 'ftsp', '$2y$10$9pS63XqmiEjQSBGskVh7POBh/LbVnBGbCQK6gpv/sRTsShMmIrQf2', 'BEM FTSP', 'ftsp@ug.kopiadem.com', '2017-07-14', 'Minggu, 30 Juli 2017 | 09:07:35', 1, 'Active', '');
INSERT INTO `admin` VALUES (5, '6g75968b04e28240', 'psikologi', '$2y$10$9pS63XqmiEjQSBGskVh7POBh/LbVnBGbCQK6gpv/sRTsShMmIrQf2', 'BEM FP', 'psikologi@ug.kopiadem.com', '2017-07-14', 'Minggu, 30 Juli 2017 | 10:19:26', 1, 'Active', '');
INSERT INTO `admin` VALUES (6, 'ubo5968b15b4aa38', 'sastra', '$2y$10$9pS63XqmiEjQSBGskVh7POBh/LbVnBGbCQK6gpv/sRTsShMmIrQf2', 'BEM FS', 'sastra@ug.kopiadem.com', '2017-07-14', 'Minggu, 30 Juli 2017 | 10:23:27', 1, 'Active', '');
INSERT INTO `admin` VALUES (7, 'zrw5968b179d9829', 'kebidanan', '$2y$10$9pS63XqmiEjQSBGskVh7POBh/LbVnBGbCQK6gpv/sRTsShMmIrQf2', 'BEM FK', 'kebidanan@ug.kopiadem.com', '2017-07-14', 'Minggu, 30 Juli 2017 | 10:37:12', 1, 'Active', '');

-- ----------------------------
-- Table structure for pengguna
-- ----------------------------
DROP TABLE IF EXISTS `pengguna`;
CREATE TABLE `pengguna`  (
  `id` int(5) NOT NULL AUTO_INCREMENT,
  `pengguna_id` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `admin_id` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `username` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `hash` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `nama` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `nama_ketuplak` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `email` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `waktu_daftar` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `login_terakhir` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL DEFAULT 'never',
  `email_status` int(1) NOT NULL DEFAULT 0,
  `email_hash` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `ganti_email` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `username`(`username`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 22 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of pengguna
-- ----------------------------
INSERT INTO `pengguna` VALUES (1, '9g55974b1e732ebc', '4r35968ac9091261', 'technofair', '$2y$10$5EQRGOQ7bLQF6wZ75Gud8u/Ue59mVxIhgLYCr7EntIQ4snv8/GOzK', 'TechnoFair 2017', 'Dani Abani', 'tf.fikti@ug.kopiadem.com', '2017-07-23', 'Minggu, 22 April 2018 | 20:04:28', 1, 'Actived', '');
INSERT INTO `pengguna` VALUES (2, 'ygw597d3ce45c66e', '4r35968ac9091261', 'bios', '$2y$10$5EQRGOQ7bLQF6wZ75Gud8u/Ue59mVxIhgLYCr7EntIQ4snv8/GOzK', 'BIOS', 'Eka Swastika', 'bios.fikti@ug.kopiadem.com', '2017-07-30', 'Minggu, 30 Juli 2017 | 16:47:28', 1, 'Actived', '');
INSERT INTO `pengguna` VALUES (3, 'n5e597d3d9461da1', '4r35968ac9091261', 'heroes', '$2y$10$5EQRGOQ7bLQF6wZ75Gud8u/Ue59mVxIhgLYCr7EntIQ4snv8/GOzK', 'HEROES', 'Gilang Setiawan', 'heroes.fikti@ug.kopiadem.com', '2017-07-30', 'Minggu, 30 Juli 2017 | 17:25:08', 1, 'Actived', '');
INSERT INTO `pengguna` VALUES (4, 'wfg597d3de5d793d', 'itj5968acbfafe2d', 'fati-fair', '$2y$10$5EQRGOQ7bLQF6wZ75Gud8u/Ue59mVxIhgLYCr7EntIQ4snv8/GOzK', 'FTI Fair', 'Doni Kharisma', 'fair.fti@ug.kopiadem.com', '2017-07-30', 'never', 1, 'Actived', '');
INSERT INTO `pengguna` VALUES (5, 'eco597d3e1b84d6d', 'itj5968acbfafe2d', 'it-festival', '$2y$10$5EQRGOQ7bLQF6wZ75Gud8u/Ue59mVxIhgLYCr7EntIQ4snv8/GOzK', 'IT Festival', 'Deri Pratama', 'festival.fti@ug.kopiadem.com', '2017-07-30', 'never', 1, 'Actived', '');
INSERT INTO `pengguna` VALUES (6, 'q13597d3e6a46e7f', 'itj5968acbfafe2d', 'fti-social', '$2y$10$5EQRGOQ7bLQF6wZ75Gud8u/Ue59mVxIhgLYCr7EntIQ4snv8/GOzK', 'FTI Social', 'Irvan Kurniawan', 'social.fti@ug.kopiadem.com', '2017-07-30', 'never', 1, 'Actived', '');
INSERT INTO `pengguna` VALUES (7, '4uf597d3ef743665', 'psd5968af8a0cf08', 'kies', '$2y$10$5EQRGOQ7bLQF6wZ75Gud8u/Ue59mVxIhgLYCr7EntIQ4snv8/GOzK', 'KIES', 'Dwi Fathan', 'kies.ekonomi@ug.kopiadem.com', '2017-07-30', 'never', 1, 'Actived', '');
INSERT INTO `pengguna` VALUES (8, 'i1f597d3f1cc664e', 'psd5968af8a0cf08', 'isef', '$2y$10$5EQRGOQ7bLQF6wZ75Gud8u/Ue59mVxIhgLYCr7EntIQ4snv8/GOzK', 'ISEF', 'Ridwan Triputro', 'isef.ekonomi@ug.kopiadem.com', '2017-07-30', 'never', 1, 'Actived', '');
INSERT INTO `pengguna` VALUES (9, 'u3o597d3f4d67d2d', 'psd5968af8a0cf08', 'gaaac', '$2y$10$5EQRGOQ7bLQF6wZ75Gud8u/Ue59mVxIhgLYCr7EntIQ4snv8/GOzK', 'GAAAC', 'Naomi Ekaswati', 'gaaac.ekonomi@ug.kopiadem.com', '2017-07-30', 'never', 1, 'Actived', '');
INSERT INTO `pengguna` VALUES (10, '5do597d4f93baa44', 'veg5968afee78956', 'talkshow-ftsp', '$2y$10$5EQRGOQ7bLQF6wZ75Gud8u/Ue59mVxIhgLYCr7EntIQ4snv8/GOzK', 'Talkshow FTSP', 'Guntur Cahyadi', 'talkshow.ftsp@ug.kopiadem.com', '2017-07-30', 'never', 1, 'Actived', '');
INSERT INTO `pengguna` VALUES (11, 'onv597d4fceb2675', 'veg5968afee78956', 'seminar-ftsp', '$2y$10$5EQRGOQ7bLQF6wZ75Gud8u/Ue59mVxIhgLYCr7EntIQ4snv8/GOzK', 'Seminar FTSP', 'Handoko Kusuma', 'seminar.ftsp@ug.kopiadem.com', '2017-07-30', 'never', 1, 'Actived', '');
INSERT INTO `pengguna` VALUES (12, '5ud597d5023222f0', 'veg5968afee78956', 'workshop-ftsp', '$2y$10$5EQRGOQ7bLQF6wZ75Gud8u/Ue59mVxIhgLYCr7EntIQ4snv8/GOzK', 'Workshop FTSP', 'Budi Gunawan', 'workshop.ftsp@ug.kopiadem.com', '2017-07-30', 'never', 1, 'Actived', '');
INSERT INTO `pengguna` VALUES (13, 'lc5597d5070ea7d1', '6g75968b04e28240', 'spc', '$2y$10$5EQRGOQ7bLQF6wZ75Gud8u/Ue59mVxIhgLYCr7EntIQ4snv8/GOzK', 'SPC', 'Indra Sudjarwadi', 'spc.psikologi@ug.kopiadem.com', '2017-07-30', 'never', 1, 'Actived', '');
INSERT INTO `pengguna` VALUES (14, 'o0d597d50b5b5c7d', '6g75968b04e28240', 'sngnc', '$2y$10$5EQRGOQ7bLQF6wZ75Gud8u/Ue59mVxIhgLYCr7EntIQ4snv8/GOzK', 'SNGNC', 'Farida Yenny', 'sngnc.psikologi@ug.kopiadem.com', '2017-07-30', 'never', 1, 'Actived', '');
INSERT INTO `pengguna` VALUES (15, 'dly597d5110c2592', '6g75968b04e28240', 'cyerpsychology', '$2y$10$5EQRGOQ7bLQF6wZ75Gud8u/Ue59mVxIhgLYCr7EntIQ4snv8/GOzK', 'Cyberpsychology', 'Liana Siska', 'cyber.psikologi@ug.kopiadem.com', '2017-07-30', 'never', 1, 'Actived', '');
INSERT INTO `pengguna` VALUES (16, 'saa597d5154b2145', 'ubo5968b15b4aa38', 'seminar-sastra', '$2y$10$5EQRGOQ7bLQF6wZ75Gud8u/Ue59mVxIhgLYCr7EntIQ4snv8/GOzK', 'Seminar Sastra', 'Yuliana Indah', 'seminar.sastra@ug.kopiadem.com', '2017-07-30', 'never', 1, 'Actived', '');
INSERT INTO `pengguna` VALUES (17, 'dk3597d518839e67', '', 'semnas-sastra', '$2y$10$q.QGQgzk5Sbnc2S.26JuB.64YuJH4qWwVfv3rMLrHAcQSVyhwe3Yu', 'Semnas Sastra', 'Wiranata Kusuma', 'semnas.sastra@ug.kopiadem.com', '2017-07-30', 'never', 0, 'beaf682b95cb1a4d1f8b373fca97c9806bc13540df23a9baf9f271db4c128ddb1b985721d5ca9a3932679b4d9d821e8b64c935bb92200a5227d7d1642b7a3d93', '');
INSERT INTO `pengguna` VALUES (18, 'ton597d51b93fd50', 'ubo5968b15b4aa38', 'trol', '$2y$10$HNqPz8SzdfVEkL9UHsMg6OgF4UqzXn/LojvmP1WrjPQ26lTk0csHu', 'TRoL', 'Lestari Safitri', 'trol.sastra@ug.kopiadem.com', '2017-07-30', 'never', 0, '90b83bc33972782540448764804304ec94c4e0f2dd8168f89845678869fd83ede161b0919a5b050b3eeb1b38f39661b96b81e748304a4f4d6dcc4e06a16c87f1', '');
INSERT INTO `pengguna` VALUES (19, '7de597d552dedab2', 'zrw5968b179d9829', 'seminar-kebidanan', '$2y$10$7EGD0PzmXxgTBFKv6tmS5uRFx5EAGh14fbwz6eSt4aCpd/HOXE9sK', 'Seminar Kebidanan', 'Indra Kusuma', 'seminar.kebidanan@ug.kopiadem.com', '2017-07-30', 'never', 0, '01dc1bfc1d1187b451e6683c670e2e6be0cb010bccc7c1c32c770b93c1a8b8412390adda287d9fd3813f58345bb7af4deb7c1aaf957e8e182027afb2949eb0e3', '');
INSERT INTO `pengguna` VALUES (20, 'osx597d580e03899', 'zrw5968b179d9829', 'talkshow-kebidanan', '$2y$10$kL/laxvk.OoGtsT4WsGR9uDAT.6h1kIxKNKM4FLxnCe22cfM2I2Cu', 'Talkshow Kebidanan', 'Ade Hermawan', 'talkshow.kebidanan@ug.kopiadem.com', '2017-07-30', 'never', 0, 'f7f76cd43ec4023d7b76c611f6a15d221e3e4e6e987966f74736aab128baae0e18b3ea9e7d7b2fef8385a1b10615b5b38bcd4c455bb5d47c589da4178cd80fa1', '');
INSERT INTO `pengguna` VALUES (21, '24v597d5947124bc', 'zrw5968b179d9829', 'kegiatan-sosial', '$2y$10$T8iQGstZUlXucDAfn1q4Ju1rTME/87U0jeFP47j8akFxHm9duECKy', 'Kegiatan Sosial', 'Sri Utari', 'sosial.kebidanan@ug.kopiadem.com', '2017-07-30', 'never', 0, '0af46cbb0ca7a95a3cc5e80d3b844ac993ff7aead6f74001f52d4a73ee6c3aa80dbb23073e15de52164f83e3fd30bcff9f173faae88ff5ea0a170a5f07c4c7bf', '');

-- ----------------------------
-- Table structure for pesan
-- ----------------------------
DROP TABLE IF EXISTS `pesan`;
CREATE TABLE `pesan`  (
  `id_pesan` int(11) NOT NULL AUTO_INCREMENT,
  `nama` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `email` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `pesan` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `status` int(1) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id_pesan`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of pesan
-- ----------------------------
INSERT INTO `pesan` VALUES (1, 'Deni Setiawan', 'deni.1743@gmail.com', 'Testing pesan...', 0);

-- ----------------------------
-- Table structure for peserta
-- ----------------------------
DROP TABLE IF EXISTS `peserta`;
CREATE TABLE `peserta`  (
  `id` int(5) NOT NULL AUTO_INCREMENT,
  `peserta_id` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `npm` varchar(25) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `nama` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `domisili` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `var_satu` varchar(25) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `var_dua` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `no_hp` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `email` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `tgl_lahir` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `waktu_daftar` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `waktu_aktifasi` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `status` int(1) NOT NULL DEFAULT 0,
  `hadir` int(1) NOT NULL DEFAULT 0,
  `waktu_hadir` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `acara_id` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `pengguna_id` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for posting
-- ----------------------------
DROP TABLE IF EXISTS `posting`;
CREATE TABLE `posting`  (
  `id` int(5) NOT NULL AUTO_INCREMENT,
  `posting_id` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `judul` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `waktu` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `isi` mediumtext CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `header` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `admin_id` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `pengguna_id` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `acara_id` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `posting_id`(`posting_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 5 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of posting
-- ----------------------------
INSERT INTO `posting` VALUES (1, 'a005974bb2f53f8a', 'Seminar Linux', 'Minggu, 23 Juli 2017 | 22:05:19', '<p>Acara mengenai sistem operasi linux.</p>', '/images/cover/4.jpg', '4r35968ac9091261', '9g55974b1e732ebc', 'a5h5974bb2f53e5c');
INSERT INTO `posting` VALUES (2, 'puh597da9e08ea42', 'Seminar Windows', 'Minggu, 30 Juli 2017 | 16:41:52', '<p>Acara Seminar Windows Technofair</p>', '/images/cover/5.jpg', '4r35968ac9091261', '9g55974b1e732ebc', 'rvo597da9e08e82c');
INSERT INTO `posting` VALUES (3, 'c0v597daa1e1d1f1', 'Workshop Linux', 'Minggu, 30 Juli 2017 | 16:42:54', '<p>Workshop linux technofair kalimalang</p>', '/images/cover/7.jpg', '4r35968ac9091261', '9g55974b1e732ebc', 'nni597daa1e1d02f');
INSERT INTO `posting` VALUES (4, 'rp9597dad958395e', 'Basic Overcloacking', 'Minggu, 30 Juli 2017 | 16:57:41', '<p>BIOS (Basic Introduction of Overcloacking Solution) Depok</p>', '/images/cover/5.jpg', '4r35968ac9091261', 'ygw597d3ce45c66e', '1sf597dad9583758');

-- ----------------------------
-- Table structure for superadmin
-- ----------------------------
DROP TABLE IF EXISTS `superadmin`;
CREATE TABLE `superadmin`  (
  `id` int(5) NOT NULL AUTO_INCREMENT,
  `super_id` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `username` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `hash` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `email` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `nama` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `username`(`username`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of superadmin
-- ----------------------------
INSERT INTO `superadmin` VALUES (1, 'yxa58efa4760adf7', 'admin', '$2y$10$6jxTyNHOUHfp6A2euEdpce7Y7RnKJYQ3L6tkCEVmKCifJ6no2Lw.2', 'admin@ug.kopiadem.com', 'Administrator');

SET FOREIGN_KEY_CHECKS = 1;
